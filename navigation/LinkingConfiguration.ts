import * as Linking from 'expo-linking';

export default {
  prefixes: [Linking.makeUrl('/')],
  config: {
    screens: {
      Root: {
        screens: {
          TabGroup: {
            screens: {
              TabGroupScreen: 'group',
            },
          },
          TabKukai: {
            screens: {
              TabKukaiScreen: 'kukai',
            },
          },
          TabMembers: {
            screens: {
              TabMembersScreen: 'members',
            },
          },
        },
      },
      NotFound: '*',
    },
  },
};
