import * as React from 'react'
import { StyleSheet, TouchableOpacity, Platform, Dimensions, Switch, ActivityIndicator } from 'react-native'
import AsyncStorage from '@react-native-async-storage/async-storage'
import cStyle from '../../components/style'
import { Ionicons } from '@expo/vector-icons'
import { Text, View, TextInput, Button } from '../../components/Themed'
import Timer from '../../components/Timer'
import { StackScreenProps } from '@react-navigation/stack'
import useColorScheme from '../../hooks/useColorScheme'
import moment from 'moment-timezone'
import 'moment/locale/ja'
import axios from 'axios'
moment.locale('ja')
import { RootStackParamList } from '../../types'
import { ScrollView } from 'react-native-gesture-handler'
let ios = true
if (Platform.OS === 'android') ios = false
const deviceWidth = Dimensions.get('window').width

export default function TabKusakuScreen({ navigation }: StackScreenProps<RootStackParamList, 'Root'>) {
	const theme = useColorScheme()
	const [hintA, setHintA] = React.useState(false)
	const [hintB, setHintB] = React.useState(false)
	const [ready, setReady] = React.useState(false)
	const [loading, setLoading] = React.useState(true)
	const [status, setStatus] = React.useState({} as any)
	const [hintC, setHintC] = React.useState(false)
	const [showTimer, setShowTimer] = React.useState(false)
	const [min, setMin] = React.useState('1')
	const [sec, setSec] = React.useState('0')
	const [int, setInt] = React.useState('0')
	const [total, setTotal] = React.useState(0)
	const [solo, setSolo] = React.useState(0)
	const [together, setTogether] = React.useState(0)
	const [thisWeek, setThisWeek] = React.useState(0)
	const [thisMonth, setThisMonth] = React.useState(0)
	const [allTotal, setAllTotal] = React.useState(0)
	const [allTogether, setAllTogether] = React.useState(0)
	const [allThisWeek, setAllThisWeek] = React.useState(0)
	const [allThisMonth, setAllThisMonth] = React.useState(0)
	const start = () => {
		setShowTimer(true)
	}
	const init = async () => {
		setReady(true)
		try {
			setLoading(true)
			const iId = await AsyncStorage.getItem('id')
			const nowPrefix = await AsyncStorage.getItem('nowPrefix')
			const nowAt = await AsyncStorage.getItem('nowAt')
			const data = await axios.post(`https://kukaibot.0px.io/v1/kusaku/my/status`, {
				i: iId,
				group: nowPrefix,
				at: nowAt,
			})
			console.log(data.data)
			setStatus(data.data ? data.data : {})
			setTotal(data.data.total)
			setSolo(data.data.solo)
			setTogether(data.data.together)
			setThisWeek(data.data.thisWeek)
			setThisMonth(data.data.thisMonth)
			const dataAll = await axios.post(`https://kukaibot.0px.io/v1/kusaku/all/status`, {
				i: iId,
				group: nowPrefix,
				at: nowAt,
			})
			console.log(dataAll.data)
			setAllTotal(dataAll.data.total)
			setAllTogether(dataAll.data.together)
			setAllThisWeek(dataAll.data.thisWeek)
			setAllThisMonth(dataAll.data.thisMonth)
			setLoading(false)
		} catch (e) {
			setLoading(false)
		}
		return true
	}
	if (!ready) init()
	return (
		<ScrollView style={[cStyle.container, cStyle[theme]]}>
			{showTimer ? <Timer show={showTimer} setShowTimer={setShowTimer} data={{ min, sec, int, hintA, hintB, hintC }} navigation={navigation} /> : null}
			<TouchableOpacity style={[styles.group, { backgroundColor: '#2D691C' }]} onLongPress={() => init()} onPress={() => navigation.navigate('KusakuStatus')} activeOpacity={0.8}>
				<Text style={styles.groupLabel}>あなたのデータ</Text>
				<View style={{ height: 15, backgroundColor: 'transparent' }} />
				{loading ? (
					<View style={[cStyle.componentCenter, cStyle.transparent, { height: 100 }]}>
						<ActivityIndicator size="large" color="#55cae0" />
					</View>
				) : (
					<View style={[cStyle.horizon, cStyle.componentCenter]}>
						<View style={[cStyle.transparent, cStyle.componentCenter]}>
							<Text color="white">句作数(累計)</Text>
							<Text style={cStyle.h1Text} color="white">
								{total}
							</Text>
						</View>
						<View style={{ width: 20, backgroundColor: 'transparent' }} />
						<View style={[cStyle.transparent, cStyle.componentCenter]}>
							<Text color="white">ひとりで句作: {solo}句</Text>
							<Text color="white">みんなで句作: {together}句</Text>
							<Text color="white">今週の句作数: {thisWeek}句</Text>
							<Text color="white">今月の句作数: {thisMonth}句</Text>
						</View>
					</View>
				)}
			</TouchableOpacity>
			<View style={{ height: 25 }} darkColor="#212121" />
			<View style={[styles.group, { backgroundColor: '#18495C', height: 170 }]}>
				<Text style={styles.groupLabel}>句作する</Text>
				<View style={{ height: 15, backgroundColor: 'transparent' }} />
				<View style={cStyle.horizon}>
					<View style={{ width: 10, backgroundColor: 'transparent' }} />
					<TouchableOpacity style={[cStyle.componentCenter, styles.btn]} activeOpacity={0.7} onPress={() => navigation.navigate('SoloKusakuEntrance')}>
						<Text style={styles.btnLabel}>ひとりで</Text>
					</TouchableOpacity>
					<View style={{ width: 20, backgroundColor: 'transparent' }} />
					<TouchableOpacity style={[cStyle.componentCenter, styles.btn]} activeOpacity={0.7} onPress={() => navigation.navigate('RoomKusakuEntrance')}>
						<Text style={styles.btnLabel}>みんなで</Text>
					</TouchableOpacity>
				</View>
			</View>
			<View style={{ height: 25 }} darkColor="#212121" />
			<View style={[styles.group, { backgroundColor: '#542B23' }]}>
				<Text style={styles.groupLabel}>句作タイマー</Text>
				<View style={{ height: 15, backgroundColor: 'transparent' }} />
				<View style={[cStyle.horizon, cStyle.componentCenter]}>
					<TextInput placeholder="1" style={[styles.form]} onChangeText={(t) => setMin(t)} value={min} keyboardType="number-pad" />
					<Text style={[styles.marginUnits]}>分</Text>
					<TextInput placeholder="0" style={[styles.form]} onChangeText={(t) => setSec(t)} value={sec} keyboardType="number-pad" />
					<Text style={[styles.marginUnits]}>秒</Text>
					<Text style={[styles.marginUnits]}>+</Text>
					<TextInput placeholder="0" style={[styles.form]} onChangeText={(t) => setInt(t)} value={int} keyboardType="number-pad" />
					<Text style={[styles.marginUnits]}>秒</Text>
				</View>
				<View style={{ height: 5, backgroundColor: 'transparent' }} />
				<View style={cStyle.switches}>
					<Switch
						trackColor={{ false: '#377a87', true: '#4598a8' }}
						thumbColor={'#4abad9'}
						ios_backgroundColor="#2a5c66"
						onValueChange={(v) => setHintA(v)}
						value={hintA}
						style={cStyle.switch}
					/>
					<TouchableOpacity onPress={() => setHintA(!hintA)} activeOpacity={0.9}>
						<Text style={ios ? { fontSize: 17, marginTop: 4 } : null} color="white">
							季題を表示
						</Text>
					</TouchableOpacity>
				</View>
				<View style={cStyle.switches}>
					<Switch
						trackColor={{ false: '#377a87', true: '#4598a8' }}
						thumbColor={'#4abad9'}
						ios_backgroundColor="#2a5c66"
						onValueChange={(v) => setHintB(v)}
						value={hintB}
						style={cStyle.switch}
					/>
					<TouchableOpacity onPress={() => setHintB(!hintB)} activeOpacity={0.9}>
						<Text style={ios ? { fontSize: 17, marginTop: 4 } : null} color="white">
							字題(単漢字)を表示
						</Text>
					</TouchableOpacity>
				</View>
				<View style={cStyle.switches}>
					<Switch
						trackColor={{ false: '#377a87', true: '#4598a8' }}
						thumbColor={'#4abad9'}
						ios_backgroundColor="#2a5c66"
						onValueChange={(v) => setHintC(v)}
						value={hintC}
						style={cStyle.switch}
					/>
					<TouchableOpacity onPress={() => setHintC(!hintC)} activeOpacity={0.9}>
						<Text style={ios ? { fontSize: 17, marginTop: 4 } : null} color="white">
							画像題を表示
						</Text>
					</TouchableOpacity>
				</View>
				<View style={{ height: 5, backgroundColor: 'transparent' }} />
				<Text color="white">タイマー終了後 「+」の右側にある秒数待ってから繰り返します。</Text>
				<View style={{ height: 5, backgroundColor: 'transparent' }} />
				<Button title="開始" onPress={() => start()} color="#a85545" />
			</View>
			<View style={{ height: 25 }} darkColor="#212121" />
			<View style={[styles.group, { backgroundColor: '#581C69' }]}>
				<Text style={styles.groupLabel}>みんなのデータ</Text>
				<View style={{ height: 15, backgroundColor: 'transparent' }} />
				{loading ? (
					<View style={[cStyle.componentCenter, cStyle.transparent, { height: 100 }]}>
						<ActivityIndicator size="large" color="#55cae0" />
					</View>
				) : (
					<View style={[cStyle.horizon, cStyle.componentCenter]}>
						<View style={[cStyle.transparent, cStyle.componentCenter]}>
							<Text color="white">句作数(累計)</Text>
							<Text style={cStyle.h1Text} color="white">
								{allTotal}
							</Text>
						</View>
						<View style={{ width: 20, backgroundColor: 'transparent' }} />
						<View style={[cStyle.transparent, cStyle.componentCenter]}>
							<Text color="white">みんなで句作: {allTogether}句</Text>
							<Text color="white">今週の句作数: {allThisWeek}句</Text>
							<Text color="white">今月の句作数: {allThisMonth}句</Text>
						</View>
					</View>
				)}
			</View>
			<View style={{ height: 50, backgroundColor: 'transparent' }} />
		</ScrollView>
	)
}
const styles = StyleSheet.create({
	group: {
		padding: 10,
		borderRadius: 10,
	},
	groupLabel: {
		fontSize: 20,
		fontWeight: 'bold',
		color: 'white',
	},
	btn: {
		backgroundColor: '#557E8F',
		borderRadius: 10,
		width: (deviceWidth - 100) / 2,
		height: 100,
		borderColor: '#ffffff',
		borderWidth: 1,
	},
	btnLabel: {
		color: '#E3EAEC',
		fontSize: 18,
	},
	form: {
		marginVertical: 2,
		borderWidth: 0,
		width: 50,
		padding: 10,
		borderRadius: 10,
	},
	marginUnits: {
		marginHorizontal: 10,
		color: 'white',
		fontSize: 16,
	},
})
