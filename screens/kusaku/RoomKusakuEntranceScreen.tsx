import * as React from 'react'
import { StyleSheet, TouchableOpacity, Platform, Dimensions, Switch, Modal, FlatList, RefreshControl } from 'react-native'
import AsyncStorage from '@react-native-async-storage/async-storage'
import cStyle from '../../components/style'
import { Ionicons, MaterialIcons } from '@expo/vector-icons'
import { Text, View, TextInput, Button } from '../../components/Themed'
import { StackScreenProps } from '@react-navigation/stack'
import useColorScheme from '../../hooks/useColorScheme'
import * as Progress from 'react-native-progress'
import moment from 'moment-timezone'
import 'moment/locale/ja'
import * as WebBrowser from 'expo-web-browser'
moment.locale('ja')
import { RootStackParamList, Room } from '../../types'
import axios from 'axios'
let ios = true
if (Platform.OS === 'android') ios = false
const deviceWidth = Dimensions.get('window').width
import { useKeyboard } from '../../hooks/keyboard'
import { ScrollView } from 'react-native-gesture-handler'

export default function TabKusakuScreen({ navigation }: StackScreenProps<RootStackParamList, 'SoloKusakuEntrance'>) {
	const theme = useColorScheme()
	const [keyboardHeight] = ios ? useKeyboard() : [30]
	const [ready, setReady] = React.useState(false)
	const [hintA, setHintA] = React.useState(false)
	const [hintB, setHintB] = React.useState(false)
	const [hintC, setHintC] = React.useState(false)
	const [addOne, setAddOne] = React.useState(false)
	const [showRealtime, setShowRealtime] = React.useState(true)
	const [refreshing, setRefreshing] = React.useState(false)
	const [min, setMin] = React.useState('1')
	const [sec, setSec] = React.useState('0')
	const [set, setSet] = React.useState('1')
	const [name, setName] = React.useState('')
	const [rooms, setRooms] = React.useState([] as Room[])

	const start = async () => {
		try {
			if (!name) return alert('名前を入力してください')

			const iId = await AsyncStorage.getItem('id')
			const nowPrefix = await AsyncStorage.getItem('nowPrefix')
			const nowAt = await AsyncStorage.getItem('nowAt')
			const S = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'
			const N = 10
			const kusakuRoom = Array.from(Array(N))
				.map(() => S[Math.floor(Math.random() * S.length)])
				.join('')
			const option = { min, sec, set, hintA, hintB, hintC, name, code: kusakuRoom, showRealtime }
			await AsyncStorage.setItem('lastUse', JSON.stringify({ min, sec, set, hintA, hintB, hintC, name }))
			setName('')
			setSet('1')
			setMin('1')
			setSec('1')
			setHintA(false)
			setHintB(false)
			setHintC(false)
			const data = await axios.post(`https://kukaibot.0px.io/v1/kusaku/room/make`, {
				i: iId,
				code: kusakuRoom,
				group: nowPrefix,
				at: nowAt,
				time: parseInt(min, 10) * 60 + parseInt(sec, 10),
				set,
				name,
				kigo: hintA,
				jidai: hintB,
				gazou: hintC,
			})
			if (data.data.success) setAddOne(false)
			if (data.data.success) navigation.navigate('RoomOwnerKusaku', { data: option, showRealtime })
		} catch (e) {}
		return true
	}
	const restore = async () => {
		try {
			const str = await AsyncStorage.getItem('lastUse')
			if (!str) alert('データはありません')
			const data = JSON.parse(str || '[]')
			let { name } = data
			const m = name.match(/\(([0-9]+)\)$/)
			if (m) {
				//すでにカッコがついてる
				name = name.navigate(/^(.+)\(([0-9]+)\)$/, `$1(${parseInt(m[1], 10) + 1})`)
			} else {
				name = `${name}(1)`
			}

			setName(name)
			setSet(data.set)
			setMin(data.min)
			setSec(data.sec)
			setHintA(data.hintA)
			setHintB(data.hintB)
			setHintC(data.hintC)
		} catch (e) {}
		return true
	}
	const navigationToRoom = async (item: Room) => {
		if (item.stage === 1) navigation.navigate('RoomKusaku', { data: item, showRealtime })
		if (item.stage !== 1) await onSen(item.kukaiId)
	}
	async function onSen(id: number) {
		setRefreshing(true)
		const nowPrefix = await AsyncStorage.getItem('nowPrefix')
		const nowAt = await AsyncStorage.getItem('nowAt')
		const iId = await AsyncStorage.getItem('id')
		if (nowPrefix && nowAt) {
			const data = await axios.post(`https://kukaibot.0px.io/v1/connect`, {
				i: iId,
				group: nowPrefix,
				at: nowAt,
				id: id,
                isKusaku: true
			})
			const code = data.data.code
			if (!code && data.data.status) {
				alert(data.data.status)
				return false
			}
			setRefreshing(false)
			await WebBrowser.openBrowserAsync(`https://kukaibot.0px.io/online/${nowPrefix}/${id}/menu?code=${code}`)
		}
	}
	const renderItem = function (item: Room) {
		return (
			<TouchableOpacity onPress={() => navigationToRoom(item)} style={[styles.room, cStyle[theme]]} activeOpacity={0.5}>
				<View style={[cStyle.horizon, cStyle.transparent, { alignItems: 'center' }]}>
					<Text style={styles.roomName}>{item.name}</Text>
					<View style={{ width: 10, backgroundColor: 'transparent' }} />
					<Ionicons name="md-people" size={16} color={theme === 'light' ? '#000000' : '#ffffff'} />
					<View style={{ width: 2, backgroundColor: 'transparent' }} />
					<Text>{item.member}</Text>
				</View>
				<View style={styles.labelGroup}>
					{item.stage === 1 ? <Text style={[{ backgroundColor: '#87a834', paddingTop: ios ? 5 : 0 }, cStyle.label]}>開催中</Text> : null}
					{item.stage === 2 ? <Text style={[{ backgroundColor: '#75344a' }, cStyle.label]}>選受付中</Text> : null}
					{item.stage === 4 ? <Text style={[{ backgroundColor: '#ba783a' }, cStyle.label]}>結果発表中</Text> : null}
				</View>
			</TouchableOpacity>
		)
	}
	const init = async (initial?: boolean) => {
		setReady(true)
		try {
			if (!initial) setRefreshing(true)
			const iId = await AsyncStorage.getItem('id')
			const nowPrefix = await AsyncStorage.getItem('nowPrefix')
			const nowAt = await AsyncStorage.getItem('nowAt')
			const data = await axios.post(`https://kukaibot.0px.io/v1/kusaku/room/list`, {
				i: iId,
				group: nowPrefix,
				at: nowAt,
			})
			setRooms(data.data.rooms ? data.data.rooms : [])
			setRefreshing(false)
		} catch (e) {
			setRooms([])
			setRefreshing(false)
		}
		return true
	}
	if (!ready) init(true)
	return (
		<View style={cStyle.container}>
			<TouchableOpacity style={[styles.group, cStyle.componentCenter, { backgroundColor: '#509B73', height: 170 }]} activeOpacity={0.8} onPress={() => setAddOne(!addOne)}>
				<Text style={styles.groupLabel} color="white">
					ルームを作成
				</Text>
			</TouchableOpacity>
			<View style={{ height: 15, backgroundColor: 'transparent' }} />
			<View style={[styles.group, { backgroundColor: '#9B5078' }]}>
				<Text style={styles.groupLabel} color="white">
					ルームに参加
				</Text>
				<View style={{ height: 5, backgroundColor: 'transparent' }} />
				{refreshing ? (
					<Progress.Bar width={deviceWidth - 50} borderRadius={0} animated={true} indeterminate={true} color="#525252" height={4} />
				) : (
					<View style={{ height: 6, backgroundColor: 'transparent' }} />
				)}
				<View style={{ height: 5, backgroundColor: 'transparent' }} />
				{rooms.length ? (
					<FlatList
						data={rooms}
						renderItem={({ item }) => renderItem(item as Room)}
						keyExtractor={(item) => `${item.id}`}
						refreshControl={<RefreshControl refreshing={refreshing} onRefresh={init} />}
						style={styles.roomList}
					/>
				) : (
					<View style={{ backgroundColor: 'transparent' }}>
						<Text color="white">ルームは見つかりませんでした</Text>
						<View style={{ height: 5, backgroundColor: 'transparent' }} />
						<Button title="再読み込み" icon="md-refresh" onPress={() => init()} color="#61324B" />
					</View>
				)}
			</View>
			<View style={cStyle.switches}>
				<Switch
					trackColor={{ false: '#377a87', true: '#4598a8' }}
					thumbColor={'#4abad9'}
					ios_backgroundColor="#2a5c66"
					onValueChange={(v) => setShowRealtime(v)}
					value={showRealtime}
					style={cStyle.switch}
				/>
				<TouchableOpacity onPress={() => setShowRealtime(!showRealtime)} activeOpacity={0.9}>
					<Text style={ios ? { fontSize: 17, marginTop: 4 } : null}>他の人が投句したときに通知する</Text>
				</TouchableOpacity>
			</View>
			<Modal visible={addOne} animationType="slide">
				<View style={[{ height: 80, backgroundColor: 'transparent' }, cStyle[theme]]} />
				<TouchableOpacity onPress={() => setAddOne(false)} style={[{ padding: 10, paddingLeft: deviceWidth - 50 }, cStyle[theme]]} activeOpacity={0.8}>
					<MaterialIcons name="close" size={30} color={theme === 'light' ? '#000000' : '#ffffff'} />
				</TouchableOpacity>
				<ScrollView style={[cStyle.container, cStyle[theme], { width: deviceWidth }]}>
					<Button title="前回と同じ条件でスタート" onPress={() => restore()} />
					<Text style={styles.groupLabel}>ルームの名前</Text>
					<TextInput placeholder="ルームの名前" style={[styles.form, styles.long]} onChangeText={(t) => setName(t)} value={name} />
					<View style={{ height: 5, backgroundColor: 'transparent' }} />
					<Text style={styles.groupLabel}>1セットあたりの時間</Text>
					<View style={{ height: 15, backgroundColor: 'transparent' }} />
					<View style={[cStyle.horizon, cStyle.componentCenter]}>
						<TextInput placeholder="1" style={[styles.form]} onChangeText={(t) => setMin(t)} value={min} keyboardType="number-pad" />
						<Text style={[styles.marginUnits]}>分</Text>
						<TextInput placeholder="0" style={[styles.form]} onChangeText={(t) => setSec(t)} value={sec} keyboardType="number-pad" />
						<Text style={[styles.marginUnits]}>秒</Text>
					</View>
					<View style={{ height: 5, backgroundColor: 'transparent' }} />
					<Text style={styles.groupLabel}>セット数</Text>
					<View style={{ height: 15, backgroundColor: 'transparent' }} />
					<View style={[cStyle.horizon, cStyle.componentCenter]}>
						<TextInput placeholder="1" style={[styles.form]} onChangeText={(t) => setSet(t)} value={set} keyboardType="number-pad" />
						<Text style={[styles.marginUnits]}>回</Text>
					</View>
					<View style={{ height: 5, backgroundColor: 'transparent' }} />
					<Text style={styles.groupLabel}>お題</Text>
					<View style={cStyle.switches}>
						<Switch
							trackColor={{ false: '#377a87', true: '#4598a8' }}
							thumbColor={'#4abad9'}
							ios_backgroundColor="#2a5c66"
							onValueChange={(v) => setHintA(v)}
							value={hintA}
							style={cStyle.switch}
						/>
						<TouchableOpacity onPress={() => setHintA(!hintA)} activeOpacity={0.9}>
							<Text style={ios ? { fontSize: 17, marginTop: 4 } : null}>季題を表示</Text>
						</TouchableOpacity>
					</View>
					<View style={cStyle.switches}>
						<Switch
							trackColor={{ false: '#377a87', true: '#4598a8' }}
							thumbColor={'#4abad9'}
							ios_backgroundColor="#2a5c66"
							onValueChange={(v) => setHintB(v)}
							value={hintB}
							style={cStyle.switch}
						/>
						<TouchableOpacity onPress={() => setHintB(!hintB)} activeOpacity={0.9}>
							<Text style={ios ? { fontSize: 17, marginTop: 4 } : null}>字題(単漢字)を表示</Text>
						</TouchableOpacity>
					</View>
					<View style={cStyle.switches}>
						<Switch
							trackColor={{ false: '#377a87', true: '#4598a8' }}
							thumbColor={'#4abad9'}
							ios_backgroundColor="#2a5c66"
							onValueChange={(v) => setHintC(v)}
							value={hintC}
							style={cStyle.switch}
						/>
						<TouchableOpacity onPress={() => setHintC(!hintC)} activeOpacity={0.9}>
							<Text style={ios ? { fontSize: 17, marginTop: 4 } : null}>画像題を表示</Text>
						</TouchableOpacity>
					</View>
					<View style={{ height: 5, backgroundColor: 'transparent' }} />
					<Button title="ルームを作成して開始" onPress={() => start()} />
					<View style={{height: keyboardHeight, backgroundColor: 'transparent'}} />
				</ScrollView>
			</Modal>
		</View>
	)
}
const styles = StyleSheet.create({
	groupLabel: {
		fontSize: 20,
		fontWeight: 'bold',
		textAlign: 'center',
	},
	form: {
		marginVertical: 2,
		borderWidth: 0,
		width: 50,
		padding: 10,
		borderRadius: 10,
	},
	marginUnits: {
		marginHorizontal: 10,
		fontSize: 16,
	},
	group: {
		padding: 10,
		borderRadius: 10,
	},
	long: {
		width: deviceWidth - 160,
	},
	room: {
		width: deviceWidth - 60,
		borderRadius: 5,
		padding: 5,
		marginVertical: 3,
	},
	roomName: {},
	labelGroup: {
		flex: 0,
		flexDirection: 'row',
		backgroundColor: 'transparent',
		paddingBottom: 5,
	},
	roomList: {
		borderRadius: 5,
		width: deviceWidth - 60,
		minHeight: 220,
	},
})
