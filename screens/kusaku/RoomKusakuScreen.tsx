import * as React from 'react'
import { StyleSheet, Platform, Dimensions, Image } from 'react-native'
import { promise as Alert } from '../../components/alert'
import AsyncStorage from '@react-native-async-storage/async-storage'
import axios from 'axios'
import cStyle from '../../components/style'
import { Audio } from 'expo-av'
import { Ionicons } from '@expo/vector-icons'
import { Text, View, TextInput, Button } from '../../components/Themed'
import { StackScreenProps } from '@react-navigation/stack'
import useColorScheme from '../../hooks/useColorScheme'
import * as Progress from 'react-native-progress'
import { RootStackParamList, Room } from '../../types'
import { useKeyboard } from '../../hooks/keyboard'
import * as WebBrowser from 'expo-web-browser'
let ios = true
if (Platform.OS === 'android') ios = false
const deviceWidth = Dimensions.get('window').width

export default function RoomKusakuScreen({ navigation, route }: StackScreenProps<RootStackParamList, 'RoomKusaku'>) {
	let data: Room = {} as any
	let showRealtime: boolean = false
	if (route.params) {
		data = route.params.data
		showRealtime = route.params.showRealtime
	}
	const theme = useColorScheme()
	const [keyboardHeight] = ios ? useKeyboard() : [0]
	const { odai } = data
	const { kigo, jidai, gazou } = odai
	const [last, setLast] = React.useState(0)
	const [init, setInit] = React.useState(false)
	const [loading, setLoading] = React.useState(false)
	const [ended, setEnded] = React.useState(false)
	const [timerStarted, setTimerStarted] = React.useState(false)
	const [i, setI] = React.useState(0)
	const [memberCt, setMemberCt] = React.useState(1)
	const [ku, setKu] = React.useState('')
	const [message, setMessage] = React.useState('　')
	const [hint, setHint] = React.useState({} as any)
	const [ws, setWs] = React.useState({} as WebSocket)
	const [forceRemove, setForceRemove] = React.useState(false)
	let commonTimer: any
	const close = async () => {
		await AsyncStorage.setItem('stopTimer', 'true')
		await AsyncStorage.removeItem('kusakuRoom')
		clearInterval(commonTimer)
	}
	React.useEffect(
		() =>
			navigation.addListener('beforeRemove', async (e) => {
				e.preventDefault()
				await AsyncStorage.setItem('safe', 'true')
				if (forceRemove) {
					ws.close()
					close()
					navigation.dispatch(e.data.action)
					return true
				}

				const a = await Alert('句作を終了しますか？', `退出(戻る)ボタンを押した、既定のセットを終了した、通信状況が悪いなどが原因です。`, ['キャンセル', '終了'])
				if (a === 1) {
					ws.close()
					setTimeout(async () => await AsyncStorage.removeItem('safe'), 1000)
					close()
					navigation.dispatch(e.data.action)
				} else {
					await AsyncStorage.removeItem('safe')
				}

			}),
		[navigation, ws, close, forceRemove]
	)
	const isStop = async () => {
		return await AsyncStorage.getItem('stopTimer')
	}
	const goBack = async (force: boolean) => {
		setForceRemove(force)
		navigation.goBack()
	}
	const startTimer = async (time: number) => {
		setInit(true)
		setTimerStarted(true)
		setEnded(false)
		const end = new Audio.Sound()
		await end.loadAsync(require('../../assets/audio/end.mp3'))
		await AsyncStorage.removeItem('stopTimer')
		if (kigo || jidai || gazou) {
			try {
				const odai = await axios.get(`https://kukaibot.0px.io/v1/kusaku/odai?kigo=${kigo}&jidai=${jidai}&gazou=${gazou}`)
				setHint(odai.data)
			} catch (e) {
				alert(e)
			}
		}
		setLast(time)
		commonTimer = setInterval(async () => {
			if (await isStop()) {
				clearInterval(commonTimer)
				await AsyncStorage.removeItem('stopTimer')
			}
			time = time - 1
			setLast(time)
			if (time < 1) {
				setEnded(true)
				clearInterval(commonTimer)
				await end.playAsync()
			}
		}, 1000)
	}
	const post = async () => {
		try {
			const provider = await AsyncStorage.getItem('provider')
			const iId = await AsyncStorage.getItem('id')
			const nowPrefix = await AsyncStorage.getItem('nowPrefix')
			const nowAt = await AsyncStorage.getItem('nowAt')
			if (provider && iId) {
				setLoading(true)
				const kukais = await axios.post(`https://kukaibot.0px.io/v1/kusaku/post`, {
					i: iId,
					group: nowPrefix,
					at: nowAt,
					haiku: ku,
					room: data.code,
				})
				setKu('')
				ws.send(JSON.stringify({ target: data.code, content: '', type: 'kusaku' }))
				setLoading(false)
				return true
			}
		} catch { }
	}
	const websocket = async () => {
		setInit(true)
		const iId = await AsyncStorage.getItem('id')
		const nowPrefix = await AsyncStorage.getItem('nowPrefix')
		const nowAt = await AsyncStorage.getItem('nowAt')
		const wss = new WebSocket(`https://kukaibot.0px.io/v1/streaming?target=${data.code}&i=${iId}&group=${nowPrefix}&at=${nowAt}`)

		setWs(wss)
		let ctShow = 0
		wss.onmessage = async (e) => {
			const message = JSON.parse(e.data)
			const { type } = message
			if ((showRealtime && type === 'kusaku') || type !== 'kusaku') setMessage(message.text)
			ctShow = 0
			if (type === 'addUser' || type === 'removeUser') setMemberCt(message.count)
			if (message.data === 'close') {
				alert('このルームは管理者によって終了されました。')
				goBack(true)
			}
			if (type === 'start') {
				startTimer(message.data.useTimeSec)
				setI(message.data.n)
			}
			if (type === 'end') await AsyncStorage.setItem('stopTimer', 'true')
			if (type === 'startSen') onSen(message.data)
		}
		wss.onclose = async () => {
			if (await AsyncStorage.getItem('safe')) return false

			const a = await Alert('ストリーミングが切断されました', `再接続を押すともう一度接続します。`, ['そのまま', '再接続'])
			if (a === 1) {
				websocket()
			} else {
				goBack(true)
			}
		}
		setInterval(() => (ctShow++ === 5 ? setMessage('　') : false), 1000)
	}
	async function onSen(id: string) {
		setLoading(true)
		try {
			const nowPrefix = await AsyncStorage.getItem('nowPrefix')
			const nowAt = await AsyncStorage.getItem('nowAt')
			const iId = await AsyncStorage.getItem('id')
			if (nowPrefix && nowAt) {
				const data = await axios.post(`https://kukaibot.0px.io/v1/connect`, {
					i: iId,
					group: nowPrefix,
					at: nowAt,
					id: id,
					isKusaku: true,
				})
				const code = data.data.code
				if (!code && data.data.status) {
					alert(data.data.status)
					return false
				}
				setLoading(false)
				await WebBrowser.openBrowserAsync(`https://kukaibot.0px.io/online/${nowPrefix}/${id}/menu?code=${code}`)
				goBack(true)
			}
		} catch (e) {
			console.error(e)
		}
	}
	if (!init) websocket()
	if (!timerStarted) {
		return (
			<View style={cStyle.center}>
				<Text>開始までしばらくお待ちください。</Text>
				<Text>進行中のセットがある場合は次のセットから参加できます。</Text>
				<View style={[cStyle.horizon, cStyle.componentCenter]}>
					<Ionicons name="md-people" size={18} color={theme === 'light' ? '#000000' : '#ffffff'} />
					<View style={{ width: 5, backgroundColor: 'transparent' }} />
					<Text style={styles.status}>{memberCt}</Text>
				</View>
				<Text>{message}</Text>
				<View style={{ height: 25, backgroundColor: 'transparent' }} />
				<Button title="終了" onPress={() => goBack(false)} color="#505050" icon="md-exit" />
			</View>
		)
	}
	return (
		<View style={cStyle.center}>
			<View style={cStyle.center}>
				{hint.type === 'image' ? <Image source={{ uri: hint.content }} style={styles.image} /> : null}
				{hint.type === 'text' ? <Text style={cStyle.kigoText}>{hint.content}</Text> : null}
				<View style={cStyle.horizon}>
					<Text style={styles.digit}>{Math.floor(last / 60) < 10 ? `0${Math.floor(last / 60)}` : Math.floor(last / 60)}</Text>
					<Text style={[styles.digit, { width: 40 }]}>:</Text>
					<Text style={styles.digit}>{last % 60 < 10 ? `0${last % 60}` : last % 60}</Text>
				</View>
				<View style={[cStyle.horizon, cStyle.componentCenter]}>
					<Text style={styles.status}>{`セット: ${i}/${data.set !== 0 ? data.set : '?'}`}</Text>
					<View style={{ width: 10, backgroundColor: 'transparent' }} />
					<Ionicons name="md-people" size={18} color={theme === 'light' ? '#000000' : '#ffffff'} />
					<View style={{ width: 5, backgroundColor: 'transparent' }} />
					<Text style={styles.status}>{memberCt}</Text>
				</View>
				<Text>{message}</Text>
				<View style={{ height: 25, backgroundColor: 'transparent' }} />
				<View style={[cStyle.horizon, cStyle.componentCenter]}>
					<Button title="終了" onPress={() => goBack(false)} color="#505050" icon="md-exit" />
				</View>
			</View>
			<View style={{ paddingBottom: keyboardHeight + 15 }}>
				{loading ? <Progress.Bar width={deviceWidth - 20} borderRadius={0} animated={true} indeterminate={true} color="#525252" height={4} /> : null}
				<View style={[cStyle.horizon, cStyle.componentCenter]}>
					<TextInput placeholder="俳句を入力…" style={[styles.form]} onChangeText={(t) => setKu(t)} value={ku} />
					<View style={{ width: 10, backgroundColor: 'transparent' }} />
					<Button title="送信" onPress={() => post()} icon="md-send" />
				</View>
			</View>
		</View>
	)
}

const styles = StyleSheet.create({
	digit: {
		fontSize: 70,
		width: 100,
		textAlign: 'right',
	},
	image: {
		width: deviceWidth,
		height: 300,
		resizeMode: 'contain',
	},
	form: {
		marginVertical: 2,
		borderWidth: 0,
		width: deviceWidth - 100,
		padding: 10,
		borderRadius: 10,
	},
	status: {
		fontSize: 18,
	},
	input: {
		paddingBottom: 15,
	},
})
