import { StackScreenProps } from '@react-navigation/stack'
import * as React from 'react'
import { StyleSheet, TouchableOpacity, BackHandler, Clipboard, FlatList, ActivityIndicator, Platform, Dimensions } from 'react-native'
import AsyncStorage from '@react-native-async-storage/async-storage'
import { Text, View, TextInput, Button } from '../../components/Themed'
import { Ionicons } from '@expo/vector-icons'
import Toast from '@cutls/react-native-tiny-toast'
import cStyle from '../../components/style'

import { RootStackParamList, KusakuHaiku } from '../../types'
import axios from 'axios'
import useColorScheme from '../../hooks/useColorScheme'
let ios = true
if (Platform.OS === 'android') ios = false
export default function KusakuStatusScreen(props: StackScreenProps<RootStackParamList, 'KusakuStatus'>) {
	React.useMemo(() => {
		Toast.hide()
		return
	}, [])
	const theme = useColorScheme()
	const { navigation } = props
	BackHandler.addEventListener('hardwareBackPress', () => {
		navigation.navigate('Root', { screen: 'TabKukai' })
		return true
	})
	const [ready, setReady] = React.useState(false)
	const [mine, setMine] = React.useState(true)
	const [loading, setLoading] = React.useState(true)
	const [nowSrc, setNowSrc] = React.useState(false)
	const [kus, setKus] = React.useState([] as KusakuHaiku[])
	const [edit, setEdit] = React.useState(-1)
	const [editLine, setEditLine] = React.useState('')
	const [q, setQ] = React.useState('')
	const [page, setPage] = React.useState(1)
	const init = async (page?: number, searchQ?: string) => {
		try {
			const provider = await AsyncStorage.getItem('provider')
			const iId = await AsyncStorage.getItem('id')
			const nowPrefix = await AsyncStorage.getItem('nowPrefix')
			const nowAt = await AsyncStorage.getItem('nowAt')
			if (provider && iId) {
				setReady(true)
				setLoading(true)
				const kukais = await axios.post(`https://kukaibot.0px.io/v1/kusaku/my`, {
					i: iId,
					group: nowPrefix,
					at: nowAt,
					page: page ? page : 1,
					q: searchQ ? searchQ : null,
				})
				console.log(searchQ)
				setKus(kukais.data.haiku)
				setLoading(false)
				setNowSrc(searchQ ? true : false)
				return true
			}
			navigation.replace('Login')
			setReady(true)
		} catch (e) {
			console.log(e)
		}
	}
	if (!ready) init()
	const renderItem = function (item: KusakuHaiku, index: number) {
		if (item.text == '') return null
		return (
			<View key={item.id}>
				<View style={styles.kukai}>
					{edit != index ? (
						<TouchableOpacity
							onPress={() => {
								Clipboard.setString(item.text)
								Toast.show('コピーしました', {
									containerStyle: { backgroundColor: theme == 'dark' ? 'rgba(255, 255, 255, 0.7)' : 'rgba(0, 0, 0, 0.7)', bottom: 12 },
									textStyle: { color: theme == 'dark' ? 'black' : 'white', fontSize: 13 },
								})
							}}
							activeOpacity={0.5}>
							<Text style={styles.title}>{item.text}</Text>
							<View style={[cStyle.horizon]}>
								{item.solo ? (
									<Text style={[{ backgroundColor: '#87a834', paddingTop: ios ? 5 : 0 }, cStyle.label]}>ひとりで</Text>
								) : (
									<Text style={[{ backgroundColor: '#5534A8', paddingTop: ios ? 5 : 0 }, cStyle.label]}>みんなで</Text>
								)}
							</View>
						</TouchableOpacity>
					) : (
						<View style={{ width: '95%' }}>
							<TextInput
								placeholder="俳句"
								style={styles.form}
								onChangeText={(text) => {
									setEditLine(text)
								}}
								value={editLine}
							/>
							<Button title="編集" onPress={() => editComplete(index, false)} materialIcon="edit" />
						</View>
					)}
				</View>
				<View style={styles.separator} lightColor="#d6d6d6" darkColor="rgba(255,255,255,0.1)" />
			</View>
		)
	}
	if (loading) {
		return (
			<View style={cStyle.center}>
				<ActivityIndicator size="large" color="#55cae0" />
			</View>
		)
	}
	const goToPage = (diff: number) => {
		const newPage = page + diff
		setPage(newPage)
		init(newPage)
	}
	const search = (reset?: boolean) => {
		setPage(1)
		if (reset) setQ('')
		init(1, reset ? undefined : q)
	}
	return (
		<View style={cStyle.container}>
			<View style={[cStyle.horizon, cStyle.componentCenter]}>
				<TextInput placeholder="語句を入力…" style={[styles.src]} onChangeText={(t) => setQ(t)} value={q} />
				<View style={{ width: 10, backgroundColor: 'transparent' }} />
				<Button title="検索" onPress={() => search()} icon="md-search" />
				{nowSrc ? <Button title="リセット" onPress={() => search(true)} icon="md-close" color="#9D0C0C" /> : null}
			</View>
			<Text>{false ? '句をタップして編集, 長押しして削除' : '句をタップしてコピー'}</Text>
			{page !== 1 ? <Button title="前のページ" onPress={() => goToPage(-1)} /> : null}
			<FlatList data={kus} renderItem={({ item, index }) => renderItem(item as KusakuHaiku, index)} keyExtractor={(item, index) => item.id} />
			{kus.length === 40 ? <Button title="次のページ" onPress={() => goToPage(1)} /> : null}
		</View>
	)
	async function editComplete(num: number, del: boolean) {
		setEdit(-1)
		const kusClone = kus
		kusClone[num].text = editLine
		setLoading(true)
		const text = kusClone[num].text
		if (text === '') del = true
		const iId = await AsyncStorage.getItem('id')
		const nowPrefix = await AsyncStorage.getItem('nowPrefix')
		const nowAt = await AsyncStorage.getItem('nowAt')
		const challenge = await axios.post(`https://kukaibot.0px.io/v1/kusaku/edit`, {
			id: kusClone[num].id,
			haiku: text,
			delete: del,
			i: iId,
			group: nowPrefix,
			at: nowAt,
		})
		setLoading(false)
		if (challenge.data.status) alert(challenge.data.status)
		setKus(kusClone)
		setEditLine('')
		return true
	}

}
const deviceWidth = Dimensions.get('window').width
const styles = StyleSheet.create({
	title: {
		fontSize: 20,
		fontWeight: 'bold',
	},
	form: {
		marginVertical: 2,
		borderWidth: 1,
		width: '100%',
		padding: 10,
		borderRadius: 10,
	},
	textarea: {
		marginVertical: 2,
		borderWidth: 1,
		width: '95%',
		padding: 10,
		borderRadius: 1,
	},
	switch: {
		transform: [{ scaleX: 1.2 }, { scaleY: 1.2 }],
		marginRight: 5,
		position: 'relative',
		top: -3,
	},
	switches: {
		marginVertical: 10,
		flex: 0,
		flexDirection: 'row',
	},
	kukai: {
		padding: 15,
		marginBottom: 0,
		flex: 0,
		flexDirection: 'row',
		width: '95%',
	},
	separator: {
		height: 1,
		width: '100%',
	},
	src: {
		marginVertical: 2,
		borderWidth: 0,
		width: deviceWidth - 150,
		maxWidth: 300,
		padding: 10,
		borderRadius: 10,
	},
})
