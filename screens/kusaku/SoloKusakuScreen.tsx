import * as React from 'react'
import { StyleSheet, Platform, Dimensions, Image, ActivityIndicator } from 'react-native'
import { promise as Alert, UNSAVE } from '../../components/alert'
import AsyncStorage from '@react-native-async-storage/async-storage'
import axios from 'axios'
import cStyle from '../../components/style'
import { Audio } from 'expo-av'
import { Text, View, TextInput, Button } from '../../components/Themed'
import { StackScreenProps } from '@react-navigation/stack'
import useColorScheme from '../../hooks/useColorScheme'
import * as Progress from 'react-native-progress'
import { RootStackParamList, SoloKusaku } from '../../types'
import { useKeyboard } from '../../hooks/keyboard'
let ios = true
if (Platform.OS === 'android') ios = false
const deviceWidth = Dimensions.get('window').width

export default function TabKusakuScreen({ navigation, route }: StackScreenProps<RootStackParamList, 'SoloKusaku'>) {
	let data: SoloKusaku = {} as any
	if (route.params) {
		data = route.params.data
	}
	const theme = useColorScheme()
	const [keyboardHeight] = ios ? useKeyboard() : [0]
	React.useEffect(
		() =>
			navigation.addListener('beforeRemove', async (e) => {
				e.preventDefault()
				const a = await Alert('句作の途中 or 既定のセットが終了しました', `句作を終了しますか？`, ['キャンセル', '終了'])
				if (a === 1) {
					close()
					navigation.dispatch(e.data.action)
				}
			}),
		[navigation]
	)
	const { hintA, hintB, hintC } = data
	const [last, setLast] = React.useState(0)
	const [init, setInit] = React.useState(false)
	const [odaiLoading, setOdaiLoading] = React.useState(false)
	const [loading, setLoading] = React.useState(false)
	const [i, setI] = React.useState(0)
	const [ku, setKu] = React.useState('')
	const [hint, setHint] = React.useState({} as any)
	let commonTimer: any
	const close = async () => {
		await AsyncStorage.setItem('stopTimer', 'true')
		await AsyncStorage.removeItem('kusakuRoom')
		clearInterval(commonTimer)
	}
	const goNext = async () => {
		close()
		if (i === parseInt(data.set, 10)) {
			navigation.goBack()
		}
		setTimeout(() => startTimer(), 1000)
	}
	const isStop = async () => {
		return await AsyncStorage.getItem('stopTimer')
	}
	const startTimer = async () => {
		const n = i + 1
		setI(n)
		setInit(true)
		const end = new Audio.Sound()
		await end.loadAsync(require('../../assets/audio/end.mp3'))
		await AsyncStorage.removeItem('stopTimer')
		if (hintA || hintB || hintC) {
			try {
				setOdaiLoading(true)
				const odai = await axios.get(`https://kukaibot.0px.io/v1/kusaku/odai?kigo=${hintA}&jidai=${hintB}&gazou=${hintC}`)
				setHint(odai.data)
				setOdaiLoading(false)
			} catch (e) {
				alert(e)
			}
		}
		let useTimeSec = parseInt(data.min, 10) * 60 + parseInt(data.sec, 10)
		setLast(useTimeSec)
		commonTimer = setInterval(async () => {
			if (await isStop()) {
				clearInterval(commonTimer)
				await AsyncStorage.removeItem('stopTimer')
			}
			useTimeSec = useTimeSec - 1
			setLast(useTimeSec)
			if (useTimeSec < 1) {
				clearInterval(commonTimer)
				await end.playAsync()
			}
		}, 1000)
	}
	if (!init) {
		setI(0)
		startTimer()
	}
	const post = async () => {
		try {
			const provider = await AsyncStorage.getItem('provider')
			const iId = await AsyncStorage.getItem('id')
			const nowPrefix = await AsyncStorage.getItem('nowPrefix')
			const nowAt = await AsyncStorage.getItem('nowAt')
			let kusakuRoom = await AsyncStorage.getItem('kusakuRoom')
			if (!kusakuRoom) {
				const S = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'
				const N = 10
				kusakuRoom = Array.from(Array(N))
					.map(() => S[Math.floor(Math.random() * S.length)])
					.join('')
				await AsyncStorage.setItem('kusakuRoom', kusakuRoom)
			}
			if (provider && iId) {
				setLoading(true)
				const kukais = await axios.post(`https://kukaibot.0px.io/v1/kusaku/solo_post`, {
					i: iId,
					group: nowPrefix,
					at: nowAt,
					haiku: ku,
					room: kusakuRoom
				})
				setKu('')
				setLoading(false)
				return true
			}
		} catch { }
	}
	return (
		<View style={cStyle.center}>
			<View style={cStyle.center}>
				{hint.type === 'image' ? <Image source={{ uri: hint.content }} style={styles.image} /> : null}
				{hint.type === 'text' ? <Text style={cStyle.kigoText}>{hint.content}</Text> : null}
				{odaiLoading ? <View>
					<ActivityIndicator size="large" color="#55cae0" />
					<Text>題を読み込んでいます</Text>
				</View> : null}
				<View style={cStyle.horizon}>
					<Text style={styles.digit}>{Math.floor(last / 60) < 10 ? `0${Math.floor(last / 60)}` : Math.floor(last / 60)}</Text>
					<Text style={[styles.digit, { width: 40 }]}>:</Text>
					<Text style={styles.digit}>{last % 60 < 10 ? `0${last % 60}` : last % 60}</Text>
				</View>
				<Text>{`${i}/${data.set !== '0' ? data.set : '?'}`}</Text>
				<View style={{ height: 25, backgroundColor: 'transparent' }} />
				<View style={[cStyle.horizon]}>
					<Button title="次へ" onPress={() => goNext()} color="#BF7A10" icon="md-arrow-forward" />
					<View style={{ width: 10, backgroundColor: 'transparent' }} />
					<Button title="終了" onPress={() => navigation.goBack()} color="#505050" icon="md-exit" />
				</View>
			</View>

			{data.input ? (
				<View style={{ paddingBottom: keyboardHeight + 15 }}>
					{loading ? <Progress.Bar width={deviceWidth - 20} borderRadius={0} animated={true} indeterminate={true} color="#525252" height={4} /> : null}
					<View style={[cStyle.horizon, cStyle.componentCenter]}>
						<TextInput placeholder="俳句を入力…" style={[styles.form]} onChangeText={(t) => setKu(t)} value={ku} />
						<View style={{ width: 10, backgroundColor: 'transparent' }} />

						<Button title="送信" onPress={() => post()} icon="md-send" />
					</View>
				</View>
			) : null}
		</View>
	)
}

const styles = StyleSheet.create({
	digit: {
		fontSize: 70,
		width: 100,
		textAlign: 'right',
	},
	image: {
		width: deviceWidth,
		height: 300,
		resizeMode: 'contain',
	},
	form: {
		marginVertical: 2,
		borderWidth: 0,
		width: deviceWidth - 100,
		padding: 10,
		borderRadius: 10,
	},
	marginUnits: {
		marginHorizontal: 10,
		fontSize: 16,
	},
})
