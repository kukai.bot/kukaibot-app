import * as React from 'react'
import { StyleSheet, TouchableOpacity, Platform, Dimensions, Switch } from 'react-native'
import AsyncStorage from '@react-native-async-storage/async-storage'
import cStyle from '../../components/style'
import { Ionicons } from '@expo/vector-icons'
import { Text, View, TextInput, Button } from '../../components/Themed'
import Timer from '../../components/Timer'
import { StackScreenProps } from '@react-navigation/stack'
import useColorScheme from '../../hooks/useColorScheme'
import moment from 'moment-timezone'
import 'moment/locale/ja'
moment.locale('ja')
import { RootStackParamList } from '../../types'
import { useKeyboard } from '../../hooks/keyboard'
import { ScrollView } from 'react-native-gesture-handler'
let ios = true
if (Platform.OS === 'android') ios = false
const deviceWidth = Dimensions.get('window').width

export default function TabKusakuScreen({ navigation }: StackScreenProps<RootStackParamList, 'SoloKusakuEntrance'>) {
	const theme = useColorScheme()
	const [keyboardHeight] = ios ? useKeyboard() : [30]
	const [hintA, setHintA] = React.useState(false)
	const [hintB, setHintB] = React.useState(false)
	const [hintC, setHintC] = React.useState(false)
	const [input, setInput] = React.useState(false)
	const [min, setMin] = React.useState('1')
	const [sec, setSec] = React.useState('0')
	const [set, setSet] = React.useState('1')
	const start = () => {
		const option = { min, sec, set, hintA, hintB, hintC, input }
		navigation.navigate('SoloKusaku', { data: option })
		return true
	}
	return (
		<ScrollView style={[cStyle.container, cStyle[theme]]}>
			<Text style={styles.groupLabel}>1セットあたりの時間</Text>
			<View style={{ height: 15, backgroundColor: 'transparent' }} />
			<View style={[cStyle.horizon, cStyle.componentCenter]}>
				<TextInput placeholder="1" style={[styles.form]} onChangeText={(t) => setMin(t)} value={min} keyboardType="number-pad" />
				<Text style={[styles.marginUnits]}>分</Text>
				<TextInput placeholder="0" style={[styles.form]} onChangeText={(t) => setSec(t)} value={sec} keyboardType="number-pad" />
				<Text style={[styles.marginUnits]}>秒</Text>
			</View>
			<View style={{ height: 5, backgroundColor: 'transparent' }} />
			<Text style={styles.groupLabel}>セット数</Text>
			<View style={{ height: 15, backgroundColor: 'transparent' }} />
			<View style={[cStyle.horizon, cStyle.componentCenter]}>
				<TextInput placeholder="1" style={[styles.form]} onChangeText={(t) => setSet(t)} value={set} keyboardType="number-pad" />
				<Text style={[styles.marginUnits]}>回</Text>
			</View>
			<View style={{ height: 5, backgroundColor: 'transparent' }} />
			<Text style={styles.groupLabel}>お題</Text>
			<View style={cStyle.switches}>
				<Switch
					trackColor={{ false: '#377a87', true: '#4598a8' }}
					thumbColor={'#4abad9'}
					ios_backgroundColor="#2a5c66"
					onValueChange={(v) => setHintA(v)}
					value={hintA}
					style={cStyle.switch}
				/>
				<TouchableOpacity onPress={() => setHintA(!hintA)} activeOpacity={0.9}>
					<Text style={ios ? { fontSize: 17, marginTop: 4 } : null}>季題を表示</Text>
				</TouchableOpacity>
			</View>
			<View style={cStyle.switches}>
				<Switch
					trackColor={{ false: '#377a87', true: '#4598a8' }}
					thumbColor={'#4abad9'}
					ios_backgroundColor="#2a5c66"
					onValueChange={(v) => setHintB(v)}
					value={hintB}
					style={cStyle.switch}
				/>
				<TouchableOpacity onPress={() => setHintB(!hintB)} activeOpacity={0.9}>
					<Text style={ios ? { fontSize: 17, marginTop: 4 } : null}>字題(単漢字)を表示</Text>
				</TouchableOpacity>
			</View>
			<View style={cStyle.switches}>
				<Switch
					trackColor={{ false: '#377a87', true: '#4598a8' }}
					thumbColor={'#4abad9'}
					ios_backgroundColor="#2a5c66"
					onValueChange={(v) => setHintC(v)}
					value={hintC}
					style={cStyle.switch}
				/>
				<TouchableOpacity onPress={() => setHintC(!hintC)} activeOpacity={0.9}>
					<Text style={ios ? { fontSize: 17, marginTop: 4 } : null}>画像題を表示</Text>
				</TouchableOpacity>
			</View>
			<View style={{ height: 5, backgroundColor: 'transparent' }} />
			<Text style={styles.groupLabel}>俳句を入力する</Text>
			<View style={cStyle.switches}>
				<Switch
					trackColor={{ false: '#377a87', true: '#4598a8' }}
					thumbColor={'#4abad9'}
					ios_backgroundColor="#2a5c66"
					onValueChange={(v) => setInput(v)}
					value={input}
					style={cStyle.switch}
				/>
				<TouchableOpacity onPress={() => setInput(!input)} activeOpacity={0.9}>
					<Text style={ios ? { fontSize: 17, marginTop: 4 } : null}>入力を使用する</Text>
				</TouchableOpacity>
			</View>
			<Text>入力された俳句は自動的に保存されます。</Text>
			<View style={{ height: 5, backgroundColor: 'transparent' }} />
			<Button title="開始" onPress={() => start()} />
			<View style={{ height: keyboardHeight, backgroundColor: 'transparent' }} />
		</ScrollView>
	)
}
const styles = StyleSheet.create({
	groupLabel: {
		fontSize: 20,
		fontWeight: 'bold',
	},
	form: {
		marginVertical: 2,
		borderWidth: 0,
		width: 50,
		padding: 10,
		borderRadius: 10,
	},
	marginUnits: {
		marginHorizontal: 10,
		fontSize: 16,
	},
})
