import * as React from 'react'
import Constants from 'expo-constants'
import { StyleSheet, Platform } from 'react-native'
import AsyncStorage from '@react-native-async-storage/async-storage'
import { Text, View, TextInput, Button } from '../components/Themed'
import * as Google from 'expo-auth-session/providers/google'
import * as AppleAuthentication from 'expo-apple-authentication'
import { StackScreenProps } from '@react-navigation/stack'
import * as Notifications from 'expo-notifications'
import * as Permissions from 'expo-permissions'
import config from '../config'
import { useKeyboard } from '../hooks/keyboard'
const isStandalone = Constants.appOwnership === 'standalone'
const ios = Platform.OS === 'ios'


import axios from 'axios'
import { RootStackParamList } from '../types'
const configOfGoogle = {
	expoClientId: config.GOOGLE_EXPO,
	androidClientId: config.GOOGLE_ANDROID_STANDALONE,
	iosClientId: config.GOOGLE_IOS_STANDALONE,
	selectAccount: true
}
import Toast from '@cutls/react-native-tiny-toast'

export default function LoginScreen({ navigation }: StackScreenProps<RootStackParamList, 'Login'>) {
	const [request, response, promptAsync] = Google.useAuthRequest(configOfGoogle)
	const [keyboardHeight] = useKeyboard()
	React.useMemo(() => {
		Toast.hide()
		return
	}, [])
	const googleLogin = async () => {
		try {
			if (response?.type !== 'success') return false
			const at = response.authentication?.accessToken
			const url = `https://www.googleapis.com/oauth2/v1/userinfo?access_token=${at}`
			const profile = await axios.get(url)
			const { id } = profile.data
			await AsyncStorage.setItem('provider', 'google')
			await AsyncStorage.setItem('id', `google:${id}`)
			setId(id)
			navigation.navigate('Join', { hideHeader: true })
		} catch (e) {
			alert('Googleログインで')
		}
	}
	React.useEffect(() => {
		googleLogin()
	}, [response])
	const [continueCode, setContinueCode] = React.useState('')
	const [id, setId] = React.useState('')
	const [busy, setBusy] = React.useState(false)
	const login = async (provider: string) => {
		setBusy(true)
		try {
			if (provider == 'continue') {
				if (!continueCode) {
					alert('引き継ぎコードを入力してください')
					return false
				}
				const url = `https://kukaibot.0px.io/v1/continue`
				const join = await axios.post(url, {
					code: continueCode,
				})
				if (join.data.error) alert(join.data.status)
				const { i, prefix, name, haigo, isAdmin, isDev, at } = join.data
				if (!i) alert(JSON.stringify(join.data))
				await AsyncStorage.setItem('provider', 'continue')
				await AsyncStorage.setItem('id', i)
				await AsyncStorage.setItem('nowPrefix', prefix)
				await AsyncStorage.setItem('nowAt', at)
				setId(i)
				const grp = [
					{
						name: name,
						id: prefix,
						at: at,
						haigo: haigo,
						admin: isAdmin,
						dev: isDev,
						joined: true,
					},
				]
				const save = JSON.stringify(grp)
				await AsyncStorage.setItem('group', save)
				try {
					const { status: existingStatus } = await Permissions.getAsync(Permissions.NOTIFICATIONS)
					let finalStatus = existingStatus
					if (existingStatus !== 'granted') {
						const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS)
						finalStatus = status
					}
					if (finalStatus !== 'granted') {
						alert('通知を登録することができませんでした。「設定」より再試行できます。')
						navigation.navigate('Root')
						return
					}
					const tokenR = await Notifications.getExpoPushTokenAsync()
					const token = tokenR.data
					await AsyncStorage.setItem('notfToken', token)
					await axios.post(`https://kukaibot.0px.io/v1/setting/notification`, {
						i: i,
						group: prefix,
						at: at,
						target: 'notificationId',
						value: token,
						notificationId: null,
					})
				} catch {
					alert('通知を登録することができませんでした。「設定」より再試行できます。')
				}
				navigation.replace('Root')
			} else if (provider == 'google') {
				try {
					promptAsync()
				} catch (e: any) {
					setBusy(false)
				}
			} else if (provider == 'apple') {
			}
		} catch ({ message }) {
			alert(`${message}`)
		}
	}
	if (busy) {
		return (
			<View style={styles.container}>
				<Text style={styles.title}>ログインを実行しています</Text>
				<Button title="中断" icon="close" onPress={() => setBusy(false)} />
			</View>
		)
	}
	return (
		<View style={styles.container}>
			<Text style={styles.title}>ログイン</Text>
			<Button title="Googleでログイン" icon="logo-google" color="#2d61b5" onPress={() => login('google')} style={styles.login} />
			{ios ? (
				<AppleAuthentication.AppleAuthenticationButton
					buttonType={AppleAuthentication.AppleAuthenticationButtonType.SIGN_IN}
					buttonStyle={AppleAuthentication.AppleAuthenticationButtonStyle.BLACK}
					cornerRadius={10}
					style={[{ height: 45 }, styles.login]}
					onPress={async () => {
						try {
							const credential = await AppleAuthentication.signInAsync({
								requestedScopes: [AppleAuthentication.AppleAuthenticationScope.FULL_NAME, AppleAuthentication.AppleAuthenticationScope.EMAIL],
							})
							console.log(credential)
							await AsyncStorage.setItem('provider', 'apple')
							await AsyncStorage.setItem('id', `apple:${credential.user}`)
							setId(credential.user)
							navigation.navigate('Join')
							// signed in
						} catch (e: any) {
							if (e.code === 'ERR_CANCELED') {
								// handle that the user canceled the sign-in flow
							} else {
								// handle other errors
							}
						}
					}}
				/>
			) : null}
			<View style={styles.horizontal}>
				<TextInput placeholder="引継ぎコード" onChangeText={(text) => setContinueCode(text)} style={styles.form} value={continueCode} />
				<Button title="引き継ぐ" icon="arrow-forward" color="#2d61b5" onPress={() => login('continue')} style={{ height: 40, marginLeft: 10 }} />
			</View>
			<View style={{ height: keyboardHeight }} />
		</View>
	)
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center',
	},
	title: {
		fontSize: 35,
		fontWeight: 'bold',
	},
	form: {
		height: 40,
		width: 230,
		padding: 8,
		borderColor: 'gray',
		borderWidth: 1,
		marginVertical: 15,
	},
	login: {
		marginVertical: 2,
		width: 230,
		padding: 10,
		color: '#fff',
		borderRadius: 10,
		flex: 0,
		flexDirection: 'row',
		height: 48
	},
	loginText: { color: '#fff', fontSize: 18 },
	loginIcon: { color: '#fff', fontSize: 25, marginRight: 8 },
	horizontal: {
		flexDirection: 'row',
		flexWrap: 'wrap',
		alignItems: 'center'
	},
})
