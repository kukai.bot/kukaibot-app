import { StackScreenProps } from '@react-navigation/stack'
import * as React from 'react'
import { StyleSheet, BackHandler, StatusBar, ScrollView, TouchableOpacity, Image, SafeAreaView, Platform } from 'react-native'
import * as WebBrowser from 'expo-web-browser'
import { Text, View } from '../components/Themed'
import { Ionicons } from '@expo/vector-icons'

import { RootStackParamList } from '../types'
import Toast from '@cutls/react-native-tiny-toast'
let ios = true
if (Platform.OS === 'android') ios = false

export default function OSSScreen({ navigation }: StackScreenProps<RootStackParamList, 'OSS'>) {
	
	React.useMemo(() => {
		Toast.hide()
		return
	}, [])
	BackHandler.addEventListener('hardwareBackPress', () => {
		navigation.navigate('Root', { screen: 'TabSetting' })
		return true
	})
	const mit = function (author: string) {
		return (<Text>
{`The MIT License (MIT)

${author}

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.`}
		</Text>
		)
	}
	return (
		<View style={styles.container}>
			<ScrollView>
				<Text style={styles.title}>クレジット</Text>
				<Text>Director: OctopusDragon</Text>
				<Text>Serverside Engineer: Cutls</Text>
				<Text>Frontend Engineer: Cutls P</Text>
				<Text>Designer: Cutls, Ryuki Yoshikawa</Text>
				<Text>Android Developer: Cutls.com, Cutls</Text>
				<Text>iOS Developer: Cutls, Ryuki Yoshikawa</Text>
				<Text>Producer: 洛南高校俳句創作部</Text>
            <Text style={styles.title}>季語データについて</Text>
            <Text>季語のリストについては「きごさい」を利用しました。末筆ながら感謝申し上げます。</Text>
            <TouchableOpacity onPress={async() => await WebBrowser.openBrowserAsync('https://kigosai.sub.jp/001/%E5%9F%BA%E6%9C%AC%E5%AD%A3%E8%AA%9E700')} style={styles.linking}>
					<Text>基本季語700 - 季語と歳時記</Text>
				</TouchableOpacity>
				<Text style={styles.title}>ソースコード</Text>
				<Text>全てのソースコードはNo Licenseにより著作権法で保護されています。</Text>
				<TouchableOpacity onPress={async() => await WebBrowser.openBrowserAsync('https://gitlab.com/kukai.bot/kukaibot-app')} style={styles.linking}>
					<Text>クライアントアプリケーション</Text>
				</TouchableOpacity>
				<TouchableOpacity onPress={async() => await WebBrowser.openBrowserAsync('https://gitlab.com/kukai.bot/kukaibot-api')} style={styles.linking}>
					<Text>APIアプリケーション</Text>
				</TouchableOpacity>
				<TouchableOpacity onPress={async() => await WebBrowser.openBrowserAsync('https://gitlab.com/kukai.bot/kukaibot-pdf')} style={styles.linking}>
					<Text>PDF作成アプリケーション</Text>
				</TouchableOpacity>
				<Text style={styles.title}>連絡先</Text>
				<TouchableOpacity onPress={async() => await WebBrowser.openBrowserAsync('mailto:p@cutls.com')} style={styles.linking}>
					<Text>ソフトウェアに関するお問い合わせ: p@cutls.com</Text>
				</TouchableOpacity>
				<TouchableOpacity onPress={async() => await WebBrowser.openBrowserAsync('https://cutls.dev')} style={styles.linking}>
					<Text>Cutlsについて: https://cutls.dev</Text>
				</TouchableOpacity>
				<Text style={styles.title}>利用規約</Text>
				<Text>2020年12月27日制定</Text>
				<Text>
					このアプリケーションを利用を開始した時点で以下の条項に同意したものとします。 サーバの所在地(日本)とユーザの本籍地の法律を全て遵守することを前提としています。
					なお、ここに書かれている条項が理解できない人は、理解できる人が本人の利用可否を判断してください。 なお、この利用規約は順次変更される可能性があります。
					最新版のアプリケーションに記されている利用規約が最新のもので、当サービスを利用する全てのユーザは最新の利用規約に従ってください。
				</Text>
				<Text>
					このソフトウェアはいかなる団体・個人からも中立な立場です。これは、上記のクレジットに記されている団体・個人に対しても同様です。
					いかなる団体・個人から何らかの接触があったとしてもこれを受け入れることはありません。
					ユーザーによるソフトウェアに対する請求として認められる内容はこのソフトウェアに対するバグ修正や機能追加要望など、 全ユーザーの利益にかなうものである必要があります。
				</Text>
				<Text>
					このソフトウェアを利用する上で生じたいかなる損害についても当ソフトウェアのクレジットに記されている団体・個人は責任を負いません。
					また、このサービス・ソフトウェアはクレジットに記されている団体・個人のみで完結するものではないことに留意してください。
					例えば、Cutlsが所持するサーバとソフトウェアの回線に障害が発生するなどの可能性を否定することはできません。
				</Text>
				<Text>
					このソフトウェアを通じて送信されるコンテンツは全て送信者が責任を負います。送信されるコンテンツはすべて他人と法的・倫理的に共有可能なものである必要があります。
					利用規約に則る限り、あなたが送信したコンテンツをサービスの運営者が利用することはありません。
					当サービスを運営する人(上記クレジットに記された全ての団体・個人)は、当利用規約に違反している内容を削除し、ユーザを凍結することができます。
					運営者による処置に納得していただけない場合、お手数ですが上記連絡先に連絡してください。
				</Text>
				<Text>
					当サービスは当アプリケーションなど運営者が規定する方法以外でアクセスすることはできません。また、アプリケーションを利用していても
					サービスに著しい負荷を与えるような行為や、過度なアプリケーションの解析等は認められません。
					このアプリケーション全体でExpoに依存しています。通知サービスはExpoのサーバを経由することに同意するものとします。なお、通知はオプトアウト可能です。
					このアプリケーションはユーザーの適切な保護やその他合理的な目的のためにログを残しております。
				</Text>
				<Text>
					この利用規約は、あなたがこのサービスから退会を行った場合、または運営者があなたを退会させた場合に解除されます。
					利用規約違反について退会後に遡って追及することはありません。なお、捜査機関が関係する場合はこの限りではありません。
					このアプリケーションに関する一切の紛争(裁判所の調停手続きを含む)は、富山地方裁判所を第一審の専属的合意管轄裁判所とすることに合意したものとします。
					この利用規約が日本法に準拠していない場合は、日本法が優先されますが、準拠しないと考えられる部分以外のすべての項目の有効性は維持されます。
					日本法に縛られないユーザの利用について、利用規約とユーザの所在国の法律が合致しない場合、ユーザの所在国の法律を優先が一般的に優先されます。
				</Text>
				<Text style={styles.title}>プライバシーポリシー</Text>
				<Text>2020年12月27日制定</Text>
				<Text>
					このアプリケーションは全ての通信を標準的な方法で暗号化するよう設計されていますが、第三者に漏洩しないことを保証するものではありません。
					ユーザは自身で適切にできる範囲でセキュリティを担保してください。
				</Text>
				<Text>
					このアプリケーションに限らず、多くのアプリケーションは外部サービスに依存しています。依存サービスは以下のオープンソース一覧を閲覧することで把握可能です。
					また、通知サービスはExpoに依存し、ExpoのサーバからAndroid端末の場合Google、iOS端末の場合Appleが所有するサービスに転送され通知が配信されます。
					これらのプライバシーポリシーや利用規約も一読してください。なお、Expoへの依存をユーザは明示的に拒否することはできません。
					ログイン方法として、サードパーティログインを採用しています。いずれのアカウントを利用する場合についても、各サービスの利用規約やプライバシーポリシーを
					一読してください。また、それらのアカウントは適切に管理してください。
				</Text>
				<Text>
					同じグループのユーザには、LINEアカウントが連携されている場合そのアイコンと設定名が公開されます。また、全てのアカウントは俳号が開示されます。
					句会の特性として句会の作成者とグループの管理者は全ての俳句の作者を把握することができます。
					グループの管理者はグループの作成者を管理者として、管理者が他のユーザを管理者にしたり管理者から外したりすることができます。
					別グループのユーザには俳号、投句した俳句等のデータは開示されることはなく、またグループへの参加状態も開示されません。
				</Text>
				<Text style={styles.title}>オープンソースライセンス(クライアント)</Text>
				<Text style={styles.title}>npm</Text>
				<Text style={styles.semiTitle}>@expo/vector-icons</Text>
				{mit(`Copyright (c) 2015 Joel Arvidsson
Copyright (c) 2020 650 Industries`)}
				<Text style={styles.semiTitle}>@react-native-community/datetimepicker</Text>
				{mit(`Copyright (c) 2019 React Native Community`)}
				<Text style={styles.semiTitle}>@react-navigation/bottom-tabs</Text>
				{mit(`Copyright (c) Satyajit Sahoo <satyajit.happy@gmail.com> (https://github.com/satya164/), Michał Osadnik <micosa97@gmail.com> (https://github.com/osdnk/)`)}
				<Text style={styles.semiTitle}>@react-navigation/native</Text>
				{mit(`Copyright (c) Satyajit Sahoo <satyajit.happy@gmail.com> (https://github.com/satya164/), Michał Osadnik <micosa97@gmail.com> (https://github.com/osdnk/)`)}
				<Text style={styles.semiTitle}>@react-navigation/stack</Text>
				{mit(`Copyright (c) Satyajit Sahoo <satyajit.happy@gmail.com> (https://github.com/satya164/), Michał Osadnik <micosa97@gmail.com> (https://github.com/osdnk/)`)}
				<Text style={styles.semiTitle}>@react-native-async-storage/async-storage</Text>
				{mit(`Copyright (c) 2015-present, Facebook, Inc.`)}
				<Text style={styles.semiTitle}>axios</Text>
				{mit(`Copyright (c) 2014-present Matt Zabriskie`)}
				<Text style={styles.semiTitle}>expo, expo-app-auth, expo-asset, expo-constants, expo-font, expo-linking, expo-splash-screen, expo-status-bar, expo-web-browser, expo-notifications, expo-permissions, expo-apple-authentication, expo-web-browser</Text>
				{mit(`Copyright (c) 2015-present 650 Industries, Inc. (aka Expo)`)}
				<Text style={styles.semiTitle}>moment, moment-timezone</Text>
				{mit(`Copyright (c) JS Foundation and other contributors`)}
				<Text style={styles.semiTitle}>react</Text>
				{mit(`Copyright (c) Facebook, Inc. and its affiliates.`)}
				<Text style={styles.semiTitle}>react, react-dom, react-native</Text>
				{mit(`Copyright (c) Facebook, Inc. and its affiliates.`)}
				<Text style={styles.semiTitle}>react-native-gesture-handler</Text>
				{mit(`Copyright (c) 2016 Krzysztof Magiera`)}
				<Text style={styles.semiTitle}>react-native-image-modal</Text>
				{mit(`Copyright (c) 2015 Joel Arvidsson`)}
				<Text style={styles.semiTitle}>react-native-safe-area-context</Text>
				{mit(`Copyright (c) 2019 Th3rd Wave`)}
				<Text style={styles.semiTitle}>react-native-screens</Text>
				{mit(`Copyright (c) 2018 Krzysztof Magiera`)}
				<Text style={styles.semiTitle}>react-native-tiny-toast</Text>
				{mit(`Copyright (c) 2018 shx996`)}
				<Text style={styles.semiTitle}>react-native-web</Text>
				{mit(`Copyright (c) 2015-present, Nicolas Gallagher.
Copyright (c) 2015-present, Facebook, Inc.`)}
				<Text style={styles.semiTitle}>その他</Text>
				<Text>@babel/core</Text>
				<Text>@types/react</Text>
				<Text>@types/react-native</Text>
				<Text>jest-expo</Text>
				<Text>typescript</Text>
				<Text style={styles.title}>オープンソースライセンス(サーバーサイド: API/PDF作成)</Text>
				<Text style={styles.title}>npm</Text>
				<Text style={styles.semiTitle}>aws-sdk</Text>
				<Text>{`Apache License
                           Version 2.0, January 2004
                        http://www.apache.org/licenses/

   TERMS AND CONDITIONS FOR USE, REPRODUCTION, AND DISTRIBUTION

   1. Definitions.

      "License" shall mean the terms and conditions for use, reproduction,
      and distribution as defined by Sections 1 through 9 of this document.

      "Licensor" shall mean the copyright owner or entity authorized by
      the copyright owner that is granting the License.

      "Legal Entity" shall mean the union of the acting entity and all
      other entities that control, are controlled by, or are under common
      control with that entity. For the purposes of this definition,
      "control" means (i) the power, direct or indirect, to cause the
      direction or management of such entity, whether by contract or
      otherwise, or (ii) ownership of fifty percent (50%) or more of the
      outstanding shares, or (iii) beneficial ownership of such entity.

      "You" (or "Your") shall mean an individual or Legal Entity
      exercising permissions granted by this License.

      "Source" form shall mean the preferred form for making modifications,
      including but not limited to software source code, documentation
      source, and configuration files.

      "Object" form shall mean any form resulting from mechanical
      transformation or translation of a Source form, including but
      not limited to compiled object code, generated documentation,
      and conversions to other media types.

      "Work" shall mean the work of authorship, whether in Source or
      Object form, made available under the License, as indicated by a
      copyright notice that is included in or attached to the work
      (an example is provided in the Appendix below).

      "Derivative Works" shall mean any work, whether in Source or Object
      form, that is based on (or derived from) the Work and for which the
      editorial revisions, annotations, elaborations, or other modifications
      represent, as a whole, an original work of authorship. For the purposes
      of this License, Derivative Works shall not include works that remain
      separable from, or merely link (or bind by name) to the interfaces of,
      the Work and Derivative Works thereof.

      "Contribution" shall mean any work of authorship, including
      the original version of the Work and any modifications or additions
      to that Work or Derivative Works thereof, that is intentionally
      submitted to Licensor for inclusion in the Work by the copyright owner
      or by an individual or Legal Entity authorized to submit on behalf of
      the copyright owner. For the purposes of this definition, "submitted"
      means any form of electronic, verbal, or written communication sent
      to the Licensor or its representatives, including but not limited to
      communication on electronic mailing lists, source code control systems,
      and issue tracking systems that are managed by, or on behalf of, the
      Licensor for the purpose of discussing and improving the Work, but
      excluding communication that is conspicuously marked or otherwise
      designated in writing by the copyright owner as "Not a Contribution."

      "Contributor" shall mean Licensor and any individual or Legal Entity
      on behalf of whom a Contribution has been received by Licensor and
      subsequently incorporated within the Work.

   2. Grant of Copyright License. Subject to the terms and conditions of
      this License, each Contributor hereby grants to You a perpetual,
      worldwide, non-exclusive, no-charge, royalty-free, irrevocable
      copyright license to reproduce, prepare Derivative Works of,
      publicly display, publicly perform, sublicense, and distribute the
      Work and such Derivative Works in Source or Object form.

   3. Grant of Patent License. Subject to the terms and conditions of
      this License, each Contributor hereby grants to You a perpetual,
      worldwide, non-exclusive, no-charge, royalty-free, irrevocable
      (except as stated in this section) patent license to make, have made,
      use, offer to sell, sell, import, and otherwise transfer the Work,
      where such license applies only to those patent claims licensable
      by such Contributor that are necessarily infringed by their
      Contribution(s) alone or by combination of their Contribution(s)
      with the Work to which such Contribution(s) was submitted. If You
      institute patent litigation against any entity (including a
      cross-claim or counterclaim in a lawsuit) alleging that the Work
      or a Contribution incorporated within the Work constitutes direct
      or contributory patent infringement, then any patent licenses
      granted to You under this License for that Work shall terminate
      as of the date such litigation is filed.

   4. Redistribution. You may reproduce and distribute copies of the
      Work or Derivative Works thereof in any medium, with or without
      modifications, and in Source or Object form, provided that You
      meet the following conditions:

      (a) You must give any other recipients of the Work or
          Derivative Works a copy of this License; and

      (b) You must cause any modified files to carry prominent notices
          stating that You changed the files; and

      (c) You must retain, in the Source form of any Derivative Works
          that You distribute, all copyright, patent, trademark, and
          attribution notices from the Source form of the Work,
          excluding those notices that do not pertain to any part of
          the Derivative Works; and

      (d) If the Work includes a "NOTICE" text file as part of its
          distribution, then any Derivative Works that You distribute must
          include a readable copy of the attribution notices contained
          within such NOTICE file, excluding those notices that do not
          pertain to any part of the Derivative Works, in at least one
          of the following places: within a NOTICE text file distributed
          as part of the Derivative Works; within the Source form or
          documentation, if provided along with the Derivative Works; or,
          within a display generated by the Derivative Works, if and
          wherever such third-party notices normally appear. The contents
          of the NOTICE file are for informational purposes only and
          do not modify the License. You may add Your own attribution
          notices within Derivative Works that You distribute, alongside
          or as an addendum to the NOTICE text from the Work, provided
          that such additional attribution notices cannot be construed
          as modifying the License.

      You may add Your own copyright statement to Your modifications and
      may provide additional or different license terms and conditions
      for use, reproduction, or distribution of Your modifications, or
      for any such Derivative Works as a whole, provided Your use,
      reproduction, and distribution of the Work otherwise complies with
      the conditions stated in this License.

   5. Submission of Contributions. Unless You explicitly state otherwise,
      any Contribution intentionally submitted for inclusion in the Work
      by You to the Licensor shall be under the terms and conditions of
      this License, without any additional terms or conditions.
      Notwithstanding the above, nothing herein shall supersede or modify
      the terms of any separate license agreement you may have executed
      with Licensor regarding such Contributions.

   6. Trademarks. This License does not grant permission to use the trade
      names, trademarks, service marks, or product names of the Licensor,
      except as required for reasonable and customary use in describing the
      origin of the Work and reproducing the content of the NOTICE file.

   7. Disclaimer of Warranty. Unless required by applicable law or
      agreed to in writing, Licensor provides the Work (and each
      Contributor provides its Contributions) on an "AS IS" BASIS,
      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
      implied, including, without limitation, any warranties or conditions
      of TITLE, NON-INFRINGEMENT, MERCHANTABILITY, or FITNESS FOR A
      PARTICULAR PURPOSE. You are solely responsible for determining the
      appropriateness of using or redistributing the Work and assume any
      risks associated with Your exercise of permissions under this License.

   8. Limitation of Liability. In no event and under no legal theory,
      whether in tort (including negligence), contract, or otherwise,
      unless required by applicable law (such as deliberate and grossly
      negligent acts) or agreed to in writing, shall any Contributor be
      liable to You for damages, including any direct, indirect, special,
      incidental, or consequential damages of any character arising as a
      result of this License or out of the use or inability to use the
      Work (including but not limited to damages for loss of goodwill,
      work stoppage, computer failure or malfunction, or any and all
      other commercial damages or losses), even if such Contributor
      has been advised of the possibility of such damages.

   9. Accepting Warranty or Additional Liability. While redistributing
      the Work or Derivative Works thereof, You may choose to offer,
      and charge a fee for, acceptance of support, warranty, indemnity,
      or other liability obligations and/or rights consistent with this
      License. However, in accepting such obligations, You may act only
      on Your own behalf and on Your sole responsibility, not on behalf
      of any other Contributor, and only if You agree to indemnify,
      defend, and hold each Contributor harmless for any liability
      incurred by, or claims asserted against, such Contributor by reason
      of your accepting any such warranty or additional liability.

   END OF TERMS AND CONDITIONS

   APPENDIX: How to apply the Apache License to your work.

      To apply the Apache License to your work, attach the following
      boilerplate notice, with the fields enclosed by brackets "[]"
      replaced with your own identifying information. (Don't include
      the brackets!)  The text should be enclosed in the appropriate
      comment syntax for the file format. We also recommend that a
      file or class name and description of purpose be included on the
      same "printed page" as the copyright notice for easier
      identification within third-party archives.

   Copyright 2012-2017 Amazon.com, Inc. or its affiliates. All Rights Reserved.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.`}
				</Text>
				<Text style={styles.semiTitle}>axios</Text>
				{mit(`Copyright (c) 2014-present Matt Zabriskie`)}
				<Text style={styles.semiTitle}>dotenv</Text>
				<Text>
				{`Copyright (c) 2015, Scott Motte
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.`}
				</Text>
				<Text style={styles.semiTitle}>knex</Text>
				{mit(`Copyright (c) 2013-present Tim Griesser`)}
				<Text style={styles.semiTitle}>koa</Text>
				{mit(`Copyright (c) 2019 Koa contributors`)}
				<Text style={styles.semiTitle}>koa-body</Text>
				{mit(`Copyright (c) 2014 Charlike Mike Reagent <mameto_100@mail.bg> and Daryl Lau <daryl@weak.io>`)}
				<Text style={styles.semiTitle}>koa-router</Text>
				{mit(`Copyright (c) 2015 Alexander C. Mingoia
Copyright (c) 2019-present Nick Baugh and Koajs contributors`)}
				<Text style={styles.semiTitle}>moment, moment-timezone</Text>
				{mit(`Copyright (c) JS Foundation and other contributors`)}
				<Text style={styles.semiTitle}>mysql2</Text>
				{mit(`Copyright (c) 2016 Andrey Sidorov (sidorares@yandex.ru) and contributors`)}
				<Text style={styles.semiTitle}>node-fetch</Text>
				{mit(`Copyright (c) 2016 - 2020 Node Fetch Team`)}
				<Text style={styles.semiTitle}>pdf-merger-js</Text>
				{mit(`Copyright (c) 2018 nbesli`)}
				<Text style={styles.semiTitle}>puppeteer</Text>
				<Text>{`Apache License
                           Version 2.0, January 2004
                        http://www.apache.org/licenses/

   TERMS AND CONDITIONS FOR USE, REPRODUCTION, AND DISTRIBUTION

   1. Definitions.

      "License" shall mean the terms and conditions for use, reproduction,
      and distribution as defined by Sections 1 through 9 of this document.

      "Licensor" shall mean the copyright owner or entity authorized by
      the copyright owner that is granting the License.

      "Legal Entity" shall mean the union of the acting entity and all
      other entities that control, are controlled by, or are under common
      control with that entity. For the purposes of this definition,
      "control" means (i) the power, direct or indirect, to cause the
      direction or management of such entity, whether by contract or
      otherwise, or (ii) ownership of fifty percent (50%) or more of the
      outstanding shares, or (iii) beneficial ownership of such entity.

      "You" (or "Your") shall mean an individual or Legal Entity
      exercising permissions granted by this License.

      "Source" form shall mean the preferred form for making modifications,
      including but not limited to software source code, documentation
      source, and configuration files.

      "Object" form shall mean any form resulting from mechanical
      transformation or translation of a Source form, including but
      not limited to compiled object code, generated documentation,
      and conversions to other media types.

      "Work" shall mean the work of authorship, whether in Source or
      Object form, made available under the License, as indicated by a
      copyright notice that is included in or attached to the work
      (an example is provided in the Appendix below).

      "Derivative Works" shall mean any work, whether in Source or Object
      form, that is based on (or derived from) the Work and for which the
      editorial revisions, annotations, elaborations, or other modifications
      represent, as a whole, an original work of authorship. For the purposes
      of this License, Derivative Works shall not include works that remain
      separable from, or merely link (or bind by name) to the interfaces of,
      the Work and Derivative Works thereof.

      "Contribution" shall mean any work of authorship, including
      the original version of the Work and any modifications or additions
      to that Work or Derivative Works thereof, that is intentionally
      submitted to Licensor for inclusion in the Work by the copyright owner
      or by an individual or Legal Entity authorized to submit on behalf of
      the copyright owner. For the purposes of this definition, "submitted"
      means any form of electronic, verbal, or written communication sent
      to the Licensor or its representatives, including but not limited to
      communication on electronic mailing lists, source code control systems,
      and issue tracking systems that are managed by, or on behalf of, the
      Licensor for the purpose of discussing and improving the Work, but
      excluding communication that is conspicuously marked or otherwise
      designated in writing by the copyright owner as "Not a Contribution."

      "Contributor" shall mean Licensor and any individual or Legal Entity
      on behalf of whom a Contribution has been received by Licensor and
      subsequently incorporated within the Work.

   2. Grant of Copyright License. Subject to the terms and conditions of
      this License, each Contributor hereby grants to You a perpetual,
      worldwide, non-exclusive, no-charge, royalty-free, irrevocable
      copyright license to reproduce, prepare Derivative Works of,
      publicly display, publicly perform, sublicense, and distribute the
      Work and such Derivative Works in Source or Object form.

   3. Grant of Patent License. Subject to the terms and conditions of
      this License, each Contributor hereby grants to You a perpetual,
      worldwide, non-exclusive, no-charge, royalty-free, irrevocable
      (except as stated in this section) patent license to make, have made,
      use, offer to sell, sell, import, and otherwise transfer the Work,
      where such license applies only to those patent claims licensable
      by such Contributor that are necessarily infringed by their
      Contribution(s) alone or by combination of their Contribution(s)
      with the Work to which such Contribution(s) was submitted. If You
      institute patent litigation against any entity (including a
      cross-claim or counterclaim in a lawsuit) alleging that the Work
      or a Contribution incorporated within the Work constitutes direct
      or contributory patent infringement, then any patent licenses
      granted to You under this License for that Work shall terminate
      as of the date such litigation is filed.

   4. Redistribution. You may reproduce and distribute copies of the
      Work or Derivative Works thereof in any medium, with or without
      modifications, and in Source or Object form, provided that You
      meet the following conditions:

      (a) You must give any other recipients of the Work or
          Derivative Works a copy of this License; and

      (b) You must cause any modified files to carry prominent notices
          stating that You changed the files; and

      (c) You must retain, in the Source form of any Derivative Works
          that You distribute, all copyright, patent, trademark, and
          attribution notices from the Source form of the Work,
          excluding those notices that do not pertain to any part of
          the Derivative Works; and

      (d) If the Work includes a "NOTICE" text file as part of its
          distribution, then any Derivative Works that You distribute must
          include a readable copy of the attribution notices contained
          within such NOTICE file, excluding those notices that do not
          pertain to any part of the Derivative Works, in at least one
          of the following places: within a NOTICE text file distributed
          as part of the Derivative Works; within the Source form or
          documentation, if provided along with the Derivative Works; or,
          within a display generated by the Derivative Works, if and
          wherever such third-party notices normally appear. The contents
          of the NOTICE file are for informational purposes only and
          do not modify the License. You may add Your own attribution
          notices within Derivative Works that You distribute, alongside
          or as an addendum to the NOTICE text from the Work, provided
          that such additional attribution notices cannot be construed
          as modifying the License.

      You may add Your own copyright statement to Your modifications and
      may provide additional or different license terms and conditions
      for use, reproduction, or distribution of Your modifications, or
      for any such Derivative Works as a whole, provided Your use,
      reproduction, and distribution of the Work otherwise complies with
      the conditions stated in this License.

   5. Submission of Contributions. Unless You explicitly state otherwise,
      any Contribution intentionally submitted for inclusion in the Work
      by You to the Licensor shall be under the terms and conditions of
      this License, without any additional terms or conditions.
      Notwithstanding the above, nothing herein shall supersede or modify
      the terms of any separate license agreement you may have executed
      with Licensor regarding such Contributions.

   6. Trademarks. This License does not grant permission to use the trade
      names, trademarks, service marks, or product names of the Licensor,
      except as required for reasonable and customary use in describing the
      origin of the Work and reproducing the content of the NOTICE file.

   7. Disclaimer of Warranty. Unless required by applicable law or
      agreed to in writing, Licensor provides the Work (and each
      Contributor provides its Contributions) on an "AS IS" BASIS,
      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
      implied, including, without limitation, any warranties or conditions
      of TITLE, NON-INFRINGEMENT, MERCHANTABILITY, or FITNESS FOR A
      PARTICULAR PURPOSE. You are solely responsible for determining the
      appropriateness of using or redistributing the Work and assume any
      risks associated with Your exercise of permissions under this License.

   8. Limitation of Liability. In no event and under no legal theory,
      whether in tort (including negligence), contract, or otherwise,
      unless required by applicable law (such as deliberate and grossly
      negligent acts) or agreed to in writing, shall any Contributor be
      liable to You for damages, including any direct, indirect, special,
      incidental, or consequential damages of any character arising as a
      result of this License or out of the use or inability to use the
      Work (including but not limited to damages for loss of goodwill,
      work stoppage, computer failure or malfunction, or any and all
      other commercial damages or losses), even if such Contributor
      has been advised of the possibility of such damages.

   9. Accepting Warranty or Additional Liability. While redistributing
      the Work or Derivative Works thereof, You may choose to offer,
      and charge a fee for, acceptance of support, warranty, indemnity,
      or other liability obligations and/or rights consistent with this
      License. However, in accepting such obligations, You may act only
      on Your own behalf and on Your sole responsibility, not on behalf
      of any other Contributor, and only if You agree to indemnify,
      defend, and hold each Contributor harmless for any liability
      incurred by, or claims asserted against, such Contributor by reason
      of your accepting any such warranty or additional liability.

   END OF TERMS AND CONDITIONS

   APPENDIX: How to apply the Apache License to your work.

      To apply the Apache License to your work, attach the following
      boilerplate notice, with the fields enclosed by brackets "[]"
      replaced with your own identifying information. (Don't include
      the brackets!)  The text should be enclosed in the appropriate
      comment syntax for the file format. We also recommend that a
      file or class name and description of purpose be included on the
      same "printed page" as the copyright notice for easier
      identification within third-party archives.

	Copyright 2017 Google Inc.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.`}</Text>
				<Text style={styles.semiTitle}>その他</Text>
				<Text>@types/dotenv</Text>
				<Text>@types/knex</Text>
				<Text>@types/koa</Text>
				<Text>@types/koa-router</Text>
				<Text>@types/mysql</Text>
				<Text>@types/node-fetch</Text>
				<Text>@types/puppeteer</Text>
				<Text>ts-node</Text>
				<Text>tslint</Text>
				<Text>typescript</Text>
				<View style={styles.bottomImg}>
					<Image source={require('../assets/images/kukaibot-app.png')} style={{ width: 160, height: 60 }} />
                    <Text>(c) 2020 Cutls, Cutls P, 洛南高校俳句創作部</Text>
				</View>
			</ScrollView>
		</View>
	)
}

const styles = StyleSheet.create({
	container: {
		top: StatusBar.currentHeight,
		padding: 5,
		overflow: 'scroll',
	},
	title: {
		fontSize: 20,
		fontWeight: 'bold',
		marginVertical: 10,
	},
	semiTitle: {
		fontSize: 17,
		fontWeight: 'bold',
		marginVertical: 5,
	},
	bottomImg: {
		marginVertical: 50,
        flex: 1,
        flexDirection: 'column',
		alignItems: 'center',
		justifyContent: 'center',
	},
	linking: {
		borderWidth: 1,
		borderColor: '#e0e0e0',
		marginVertical: 5,
		padding: 2
	}
})
