# 句会bot クライアントアプリ

LICENSE: [No License](https://choosealicense.com/no-permission/)

With...
 * ReactNative
 * Expo

 iOS, Android: `master` branch  
 web: `web` branch
 
# .env like
 on .config.ts

```
const config: CONFIG = {
    GOOGLE_EXPO: <string for host.exp.exponent client id>,
    GOOGLE_STANDALONE: <string for com.cutls.kukaibot or something>
}
interface CONFIG {
	GOOGLE_EXPO: string
	GOOGLE_STANDALONE: string
}
export default config
```