const dotenv = require('dotenv')
const fs = require('fs')

dotenv.config()

const main = async () => {
    const infoRaw = process.env.PROJECT_INFO
    const clientRaw = process.env.CLIENT
    const projectInfo = JSON.parse(base64Decode(infoRaw))
    const client = JSON.parse(base64Decode(clientRaw))
    const a = {
        "project_info": projectInfo,
        "client": [client],
        "configuration_version": "1"
    }
    fs.writeFileSync('google-services.json', JSON.stringify(a))
    const temp = fs.readFileSync('config.ts.sample').toString()
    const write = temp.replace('%GOOGLE_EXPO%', process.env.GOOGLE_EXPO)
                      .replace('%GOOGLE_ANDROID_STANDALONE%', process.env.GOOGLE_ANDROID_STANDALONE)
                      .replace('%GOOGLE_IOS_STANDALONE%', process.env.GOOGLE_IOS_STANDALONE)
    fs.writeFileSync('config.ts', write)
}
main()

function base64Decode(text) {
    return Buffer.from(text, 'base64').toString()
}