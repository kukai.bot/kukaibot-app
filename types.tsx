export type RootStackParamList = {
	Root: undefined | { screen: string }
	Group: undefined | { screen: string }
	NotFound: undefined
	Login: undefined
	Join: undefined | { hideHeader: boolean }
	KukaiMake: undefined
	Post: undefined | { data: Kukai }
	MyHaiku: undefined | { data: Kukai }
	PDF: undefined | { data: Kukai }
	Text: undefined | { data: Kukai }
	Setting: undefined
	OSS: undefined
	SoloKusakuEntrance: undefined
	SoloKusaku: undefined | { data: SoloKusaku }
	RoomKusakuEntrance: undefined
	RoomOwnerKusaku: undefined | { data: RoomKukai, showRealtime: boolean }
	RoomKusaku: undefined | { data: Room, showRealtime: boolean }
	KusakuStatus: undefined
}

export type BottomTabParamList = {
	TabSetting: undefined
	TabKusaku: undefined
	TabKukai: undefined
	TabMembers: undefined
}

export type TabKukaiParamList = {
	TabKukaiScreen: undefined
}

export type TabMembersParamList = {
	TabMembersScreen: undefined
}

export type TabSettingStackParamList = {
	TabSettingScreen: undefined
}

export type TabKusakuParamList = {
	TabKusakuScreen: undefined
}

export interface Group {
	name: string
	id: string
	haigo: string
	at: string
	admin: boolean
	dev: boolean
	joined: boolean
}
export interface Kukai {
	name: string
	id: string
	deadline: string | null
	createdAt: string
	read: boolean
	write: boolean
	isMine: boolean
	deletable: boolean
	onsen: boolean
	onsenComment: string
	onsenResult: boolean
	count: string
}

export interface Haiku {
	text: string
	code: string
}
export interface KusakuHaiku {
	text: string
	id: string
	createdAt: number
	room: string
	solo: boolean
}

export interface PDF {
	revealed: boolean
	revealed_url: string
	img_url: string[]
	img_revealed_url: string[]
	url: string
}

export interface Members {
	admin: boolean
	members: Member[]
}
export interface Member {
	name: string
	userId: string
	admin: boolean
	profile: {
		displayName: string | null
		pictureUrl: string | null
	}
}

export interface Settings {
	admin: boolean
	haigo: string
	invitationCode: null | string
	notificationId: null | string
	notification: {
		oneDayBefore: boolean
		tenMinsBefore: boolean
		newKukaiNotf: boolean
		kukaiEndNotf: boolean
	}
}

export interface KusakuBase {
	min: string
	sec: string
	set: string
	hintA: boolean
	hintB: boolean
	hintC: boolean
	code?: string
}

export interface SoloKusaku extends KusakuBase {
	input: boolean
}

export interface RoomKukai extends KusakuBase {
	name: string
}

export interface Room {
	name: string
	code: string
	id: number
	owner: string
	time: number
	set: number
	nowSet: number
	kukaiId: number
	stage: number // 1: Opening, 2: Sen available, 3: Closed without sen, 4: Sen closed
	createdAt: string
	member: number
	odai: {
		kigo: boolean
		jidai: boolean
		gazou: boolean
	}
}