import * as React from 'react'
import { StyleSheet, Image, Modal, Dimensions, ActivityIndicator } from 'react-native'
import { promise as Alert } from '../components/alert'
import { Text, View, TextInput, Button } from './Themed'
import axios from 'axios'
import cStyle from './style'
import AsyncStorage from '@react-native-async-storage/async-storage'
const deviceWidth = Dimensions.get('window').width
import { Audio } from 'expo-av'
export default ({ show, setShowTimer, data: dataRaw, navigation }: any) => {

	const data = dataRaw as { min: string; sec: string; int: string; hintA: boolean; hintB: boolean; hintC: boolean }
	const { hintA, hintB, hintC } = data
	const [last, setLast] = React.useState(0)
	const [intervalNow, setIntervalNow] = React.useState(false)
	const [visible, setVisible] = React.useState(true)
	const [timerNow, setTimerNow] = React.useState(true)
	const [init, setInit] = React.useState(false)
	const [odaiLoading, setOdaiLoading] = React.useState(false)
	const [hint, setHint] = React.useState({} as any)
	let commonTimer: any
	const close = async () => {
		await AsyncStorage.setItem('stopTimer', 'true')
		setVisible(false)
		setTimerNow(false)
		clearInterval(commonTimer)
		setTimeout(() => setShowTimer(false), 500)
	}
	const isStop = async () => {
		return await AsyncStorage.getItem('stopTimer')
	}
	const getsetOdai = async () => {
		try {
			setHint({})
			setOdaiLoading(true)
			const odai = await axios.get(`https://kukaibot.0px.io/v1/kusaku/odai?kigo=${hintA}&jidai=${hintB}&gazou=${hintC}`)
			setOdaiLoading(false)
			setHint(odai.data)
		} catch (e) {
			alert(e)
		}
	}
	const onImageError = async () => {
		const i = await Alert('画像読み込みエラー', `再読み込みしますか？`, ['キャンセル', '再読み込み'])
		if (i === 1) getsetOdai()
	}
	const startTimer = async (interval: boolean) => {
		const start = new Audio.Sound()
		await start.loadAsync(require('../assets/audio/start.mp3'))
		const end = new Audio.Sound()
		await end.loadAsync(require('../assets/audio/end.mp3'))
		await AsyncStorage.removeItem('stopTimer')
		setInit(true)
		if (!timerNow) return false
		if (hintA || hintB || hintC) {
			await getsetOdai()
		}
		setIntervalNow(interval)
		const int = parseInt(data.int, 10)
		let useTimeSec = interval ? parseInt(data.int, 10) : parseInt(data.min, 10) * 60 + parseInt(data.sec, 10)
		setLast(useTimeSec)
		commonTimer = setInterval(async () => {
			if (await isStop()) {
				clearInterval(commonTimer)
				await AsyncStorage.removeItem('stopTimer')
			}
			useTimeSec = useTimeSec - 1
			setLast(useTimeSec)
			if (useTimeSec < 1) {
				clearInterval(commonTimer)
				if (interval || (!interval && int < 1)) {
					startTimer(false)
					await start.playAsync()
				}
				if (!interval && int > 0) {
					startTimer(true)
					await end.playAsync()
				}
			}
		}, 1000)
	}
	if (!init) startTimer(false)
	return (
		<Modal visible={visible} animationType="slide">
			<View style={cStyle.center}>
				{hint.type === 'image' ? <Image source={{ uri: hint.content }} style={styles.image} onError={() => onImageError()} /> : null}
				{hint.type === 'text' ? <Text style={cStyle.kigoText}>{hint.content}</Text> : null}
				{odaiLoading ? <View>
					<ActivityIndicator size="large" color="#55cae0" />
					<Text>題を読み込んでいます</Text>
				</View>: null}
				<View style={cStyle.horizon}>
					<Text style={styles.digit}>{Math.floor(last / 60) < 10 ? `0${Math.floor(last / 60)}` : Math.floor(last / 60)}</Text>
					<Text style={[styles.digit, { width: 40 }]}>:</Text>
					<Text style={styles.digit}>{last % 60 < 10 ? `0${last % 60}` : last % 60}</Text>
				</View>
				{intervalNow ? <Text>インターバル中…</Text> : <Text>　</Text>}
				<Button title="終了" onPress={() => close()} color="#505050" icon="md-exit" />
			</View>
		</Modal>
	)
}
const styles = StyleSheet.create({
	digit: {
		fontSize: 70,
		width: 100,
		textAlign: 'right',
	},
	image: {
		width: deviceWidth,
		height: 300,
		resizeMode: 'contain',
	},
})
