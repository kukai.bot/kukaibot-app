import { Alert } from 'react-native'
interface AlertButton {
    text?: string
    style?: 'default' | 'cancel' | 'destructive'
}
interface AlertButtonLegacy extends AlertButton {
    onPress?: (value?: string) => void
}
interface AlertOptions {
    /** @platform android */
    cancelable?: boolean
    /** @platform android */
    onDismiss?: () => void
}
export const UNSAVE: AlertButton[] = [{ text: 'キャンセル', style: 'cancel' }, { text: '続行', style: 'destructive' }]
export const DELETE: AlertButton[] = [{ text: 'キャンセル', style: 'cancel' }, { text: '削除', style: 'destructive' },]
export const promise = async (title: string, message: string, buttons: AlertButton[] | string[], options?: AlertOptions) => {
    return new Promise((resolve:  (value: number) => void, reject) => {
        const useButton = []
        for (let i = 0; i < buttons.length; i++) {
            const useIt = buttons[i]
            const target = typeof useIt === 'string' ? { text: useIt } : useIt
            const buttonExt: AlertButtonLegacy = target
            buttonExt.onPress = () => resolve(i)
            useButton.push(buttonExt)
        }
        Alert.alert(title, message, useButton, options)
    })
}