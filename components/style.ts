import { Platform, StyleSheet } from 'react-native'
let ios = true
if (Platform.OS === 'android') ios = false
export default StyleSheet.create({
	container: {
		flex: 1,
		padding: 20
	},
	center: {
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center',
	},
	componentCenter: {
		flex: 0,
		alignItems: 'center',
		justifyContent: 'center',
	},
	title: {
		fontSize: 20,
		fontWeight: 'bold',
	},
	horizon: {
		flex: 0,
		flexWrap: 'wrap',
		flexDirection: 'row',
		backgroundColor: 'transparent'
	},
	switch: {
		transform: [{ scaleX: ios ? 1 : 1.2 }, { scaleY: ios ? 1 : 1.2 }],
		marginRight: 5,
		position: 'relative',
		top: -3,
		backgroundColor: 'transparent'
	},
	switches: {
		marginVertical: 10,
		flex: 0,
		flexDirection: 'row',
		backgroundColor: 'transparent'
	},
	separator: {
		height: 1,
		width: '100%',
	},
	label: {
		padding: ios ? 5 : 1,
		paddingHorizontal: 5,
		borderRadius: 5,
		marginHorizontal: 5,
		marginTop: ios ? 5 : 0,
		color: 'white',
	},
	transparent: {
		backgroundColor: 'transparent'
	},
	light: {
		backgroundColor: '#ffffff'
	},
	dark: {
		backgroundColor: '#212121'
	},
	h1Text: {
		fontSize: 60
	},
	kigoText: {
		fontSize: 40
	}
})
